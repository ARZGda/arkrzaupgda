package pl.arek.animalTypeRepository;

import org.springframework.data.repository.CrudRepository;
import pl.arek.entity.Animal;

public interface AnimalRepository extends CrudRepository<Animal, Long> {

}