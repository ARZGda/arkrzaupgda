package pl.arek.animalTypeRepository;

import org.springframework.data.repository.CrudRepository;
import pl.arek.entity.AnimalType;

public interface AnimalTypeRepository extends CrudRepository<AnimalType, Long> {

}