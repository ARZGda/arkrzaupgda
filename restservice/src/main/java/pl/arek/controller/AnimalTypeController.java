package pl.arek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.arek.animalTypeRepository.AnimalTypeRepository;
import pl.arek.entity.AnimalType;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/species")
public class AnimalTypeController {

    @Autowired
    private AnimalTypeRepository animalTypeRepository;

    @RequestMapping("/add")
    public AnimalType add(@RequestParam(name = "name") String name,
                          @RequestParam(name = "description") String description) {
        AnimalType animalType = new AnimalType();
        animalType.setName(name);
        animalType.setDescription(description);
        return animalTypeRepository.save(animalType);
    }

    @RequestMapping("/show")
    public List<AnimalType> showAll() {
        return (List<AnimalType>) animalTypeRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public AnimalType showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return animalTypeRepository.findOne(myId);
    }
}