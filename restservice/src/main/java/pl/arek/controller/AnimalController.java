package pl.arek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.arek.animalTypeRepository.AnimalRepository;
import pl.arek.animalTypeRepository.AnimalTypeRepository;
import pl.arek.entity.Animal;
import pl.arek.entity.AnimalType;

import java.util.*;


@CrossOrigin
@RestController
@RequestMapping("/animals")
public class AnimalController {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private AnimalTypeRepository animalTypeRepository;

    @RequestMapping("/")
    public String animal() {
        return "";
    }

    @RequestMapping("/show")
    public List<Animal> listAnimals() {
        return (List<Animal>) animalRepository.findAll();
    }

    @RequestMapping("/add")
    public Animal addAnimal(@RequestParam(value = "name") String name,
                            @RequestParam(value = "description") String description,
                            @RequestParam(value = "image") String image,
                            @RequestParam(value = "animalType") AnimalType animalType)
    {
        Animal animal = new Animal();
        animal.setName(name);
        animal.setDescription(description);
        animal.setImage(image);
        animal.setAnimalType(animalType);
        return animalRepository.save(animal);
    }

    @RequestMapping("/show/{id}")
    public Animal showAnimalByID(@PathVariable("id") String id) {
        return animalRepository.findOne(Long.valueOf(id));
    }

}
