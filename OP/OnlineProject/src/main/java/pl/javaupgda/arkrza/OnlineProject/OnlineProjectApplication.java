package pl.javaupgda.arkrza.OnlineProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineProjectApplication.class, args);
	}
}
