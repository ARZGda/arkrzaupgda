package pl.org.gda.up;

/**
 * Created by Rzadki on 2017-06-29.
 */
public class Profile {
    private Codec codec;
    private Extension extension;
    private Resolution resolution;
    private int index;

    public Profile(Codec codec, Extension extension, Resolution resolution, int index) {
        this.codec = codec;
        this.extension = extension;
        this.resolution = resolution;
        this.index = index;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "codec=" + codec +
                ", extension=" + extension +
                ", resolution=" + resolution +
                ", index=" + index +
                '}';
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Codec getCodec() {
        return codec;
    }

    public void setCodec(Codec codec) {
        this.codec = codec;
    }

    public Extension getExtension() {
        return extension;
    }

    public void setExtension(Extension extension) {
        this.extension = extension;
    }

    public Resolution getResolution() {
        return resolution;
    }

    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }
}
