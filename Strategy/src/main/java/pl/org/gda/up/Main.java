package pl.org.gda.up;

import java.util.Scanner;

/**
 * Created by Rzadki on 2017-06-29.
 */

public class Main {
    static Scanner sc = new Scanner(System.in);
    static Scanner intSc = new Scanner(System.in);
    static private int counter;
    static ScreenRecorder scRecord = new ScreenRecorder();

    public static void main(String[] args) {
        menu();
        int a = intSc.nextInt();
        choiceFromMenu(a);
        sc.close();
        intSc.close();
    }

    private static void menu() {
        System.out.println("Co chcesz robić?\n" +
                "1. Tworzenie profilu\n" +
                "2. Wybór profilu\n" +
                "3. Lista profili\n" +
                "4. Nagrywanie\n" +
                "5. Koniec nagrywania\n" +
                "6. Zakończenie programu");
    }

    private static void choiceFromMenu(int a) {
        do {
            cases(a);
            a = intSc.nextInt();
        } while (a > 0 && a < 6);
    }

    private static void cases(int a) {
        if (a == 1) caseOne();
        if (a == 2) caseTwo();
        if (a == 3) caseThree();
        if (a == 4) caseFour();
        if (a == 5) caseFive();
    }

    private static void caseOne() {
        makeProfile();
    }

    private static void caseTwo() {
        if (counter != 0) {
            try {
                System.out.println("Podaj numer wybranego profilu :");
                int index = intSc.nextInt();
                scRecord.setProfile(index);
            } catch (IndexOutOfBoundsException ioob) {
                System.out.println("Nie ma takiego profilu, sprawdz liste!");
            }
        } else {
            System.out.println("Nie ma żadnego profilu");
        }
    }

    private static void caseThree() {
        scRecord.listProfiles();
    }

    private static void caseFour() {
        scRecord.startRecording();
    }

    private static void caseFive() {
        scRecord.stopRecording();
    }

    public static void makeProfile() {
        String codec = "", resolution = "", extension = "";
        boolean properValue;
        properValue = false;
        codec = getCodec(codec, properValue);
        properValue = false;
        resolution = getResolution(resolution, properValue);
        properValue = false;
        extension = getExtension(extension, properValue);
        counter++;

        scRecord.addProfile(codec, resolution, extension, counter);
    }

    private static String getCodec(String codec, boolean properValue) {
        while (properValue != true) {
            try {

                System.out.println("Wybierz kodek: h264, h263, theora, av8");
                codec = sc.nextLine();
                Codec.valueOf(codec);
                properValue = true;
            } catch (IllegalArgumentException iae) {

                System.out.println("Spróbuj jeszcze raz.");
            }
        }
        return codec;
    }

    private static String getResolution(String resolution, boolean properValue) {
        while (properValue != true) {
            try {

                System.out.println("Wybierz rozdzielczość: R1920x1080, R720x576, R680x550, R720x480");
                resolution = sc.nextLine();
                Resolution.valueOf(resolution);
                properValue = true;
            } catch (IllegalArgumentException iae) {

                System.out.println("Spróbuj jeszcze raz.");
            }
        }
        return resolution;
    }

    private static String getExtension(String extension, boolean properValue) {
        while (properValue != true) {
            try {
                System.out.println("Wybierz rozszerzenie pliku docelowego: mp4, mp3, mkv, avi");
                extension = sc.nextLine();
                Extension.valueOf(extension);
                properValue = true;
            } catch (IllegalArgumentException iae) {
                System.out.println("Spróbuj jeszcze raz.");
            }
        }
        return extension;
    }

}

