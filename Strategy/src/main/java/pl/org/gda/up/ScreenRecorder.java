package pl.org.gda.up;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rzadki on 2017-06-29.
 */

public class ScreenRecorder {

    private int counter = 0;
    List<Profile> profileList = new LinkedList();
    Profile currentPr = null;
    private boolean recording = false;

    public void addProfile(String readCodec, String readResolution, String readExtension, int mainCounter) {

        System.out.println("Tworzymy nowy profil.");
        counter++;

        Codec codec = Codec.valueOf(readCodec);

        Resolution resolution = Resolution.valueOf(readResolution);

        Extension extension = Extension.valueOf(readExtension);

        Profile currentProfile = new Profile(codec, extension, resolution, mainCounter);
        profileList.add(currentProfile);

    }

    public void setProfile(int index) {

        if (counter == 0) {
            System.out.println("Żaden profil nie istnieje, musisz go stworzyć!");
        } else {
            if (index > counter || index - 1 < 0) {
                System.out.println("Żądany profil nie istnieje!");
            } else {
                System.out.println(profileList.get(index - 1).toString());
                currentPr = profileList.get(index - 1);
            }
        }

    }

    public void listProfiles() {
        if (counter == 0) {
            System.out.println("Lista profili jest pusta!");
        } else {
            for (int i = 1; i <= counter; i++) {
                System.out.println(profileList.get(i - 1).toString());
            }
        }
    }

    public void startRecording() {
        if (counter == 0) {
            System.out.println("Nie ma żadnego profilu, nie mogę nagrywać.");
        } else if (currentPr == null) {
            setDefaultProfile();
        } else if (recording == false) {
            System.out.println("Uruchamiam nagrywanie");
        } else {
            System.out.println("Już nagrywam");
            recording = true;
        }
    }

    private void setDefaultProfile() {
        System.out.println("Nie wybrano żadnego profilu.");
        System.out.println("Ustawiam profil numer 1 jako domyślny.");
        currentPr = profileList.get(0);
        System.out.println("Uruchom nagrywanie ponownie.");
    }

    public void stopRecording() {

        if (recording == false) {
            System.out.println("Nie nagrywam");
        } else {
            System.out.println("Przerywam nagrywanie");
            recording = false;
        }
    }

}
