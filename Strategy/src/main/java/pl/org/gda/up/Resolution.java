package pl.org.gda.up;

/**
 * Created by RENT on 2017-06-29.
 */
public enum Resolution {

    R1920x1080, R720x576, R680x550, R720x480

}
