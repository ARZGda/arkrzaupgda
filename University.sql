CREATE DATABASE IF NOT EXISTS university;
USE university;

CREATE TABLE student (
	`id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `lastname` VARCHAR(100) NOT NULL
    );
    
    UPDATE `student` SET `name` = "Kamil" WHERE `id` = 3;
    SELECT * FROM student;