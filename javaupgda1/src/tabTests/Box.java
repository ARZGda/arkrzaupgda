package tabTests;

public class Box {

	public static void main(String[] args) {

		generateByBuilder();
	}

	private static void generateByBuilder() {
		StringBuilder builder = new StringBuilder();
		for (int i = 1; i <= 3; i++) {
			lineGenerator(builder);
		}
		System.out.println(builder.toString());

	}

	private static void lineGenerator(StringBuilder builder) {
		for (int j = 1; j <= 4; j++) {
			builder.append('*');
		}
		builder.append("\n");
	}

}