package tabTests;

import java.util.HashSet;
import java.util.Set;

public class StringDuplicates {

	public static boolean containDuplicates(String text){
		char[] charArray = text.toCharArray();
		Set <Character> set = new HashSet<>();
		for (Character character : charArray) {
		set.add(character);	
		}
		return (charArray.length != set.size());
	}
}
