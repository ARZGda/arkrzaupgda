package family.dziedziczenie;

public class Mother extends FamilyMember {

	public Mother(String name) {
		super(name);
	}

	public void introduce() {
		System.out.println("I'm a mother. My namie is " + this.getName());
	}
}
