package family;

public class Family {
	public static void main(String[] args) {
		Mother mother = new Mother("Gra�yna");
		introduce(mother);
		Father father = new Father("Janusz");
		introduce(father);
		Son son = new Son("Brajanek");
		introduce(son);
		Daughter daughter = new Daughter("D�esika");
		introduce(daughter);
	}
	
	public static void introduce (Mother mother){
		System.out.println("I'm a mother. My namie is "+mother.getName());
	}
	public static void introduce (Father father){
		System.out.println("I'm a father. My namie is "+father.getName());
	}
	public static void introduce (Son son){
		System.out.println("I'm a son. My namie is "+son.getName());
	}
	public static void introduce (Daughter daughter){
		System.out.println("I'm a daughter. My namie is "+daughter.getName());
	}
}
