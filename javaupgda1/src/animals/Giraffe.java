package animals;

public class Giraffe extends ZooMember implements Animal {
	public Giraffe(String name, String color, String color2) {
		super(name, 4, color, color2);
	}

	public Giraffe() {
	}

	public void introduce() {
		System.out.println("This is a " + this.getColor()+" and "+ this.getColor2() + " giraffe, it's name is " + this.getName() + " and it has "
				+ this.getLegs() + " legs.");
	}

	@Override
	public String makeNoise() {
		return "Ueoeoeo!";		
	}

}
