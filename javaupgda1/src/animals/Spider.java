package animals;

public class Spider extends ZooMember implements Animal{
	public Spider(String name, int legs, String color) {
		super(name, legs, color);
	}

	public Spider() {
	}

	public void introduce() {
		System.out.println("This is a " + this.getColor() + " spider, it's name is " + this.getName() + " and it has "
				+ this.getLegs() + " legs.");
	}

	@Override
	public String makeNoise() {
		return "This bug is too quiet to make noises!";
	}

}
