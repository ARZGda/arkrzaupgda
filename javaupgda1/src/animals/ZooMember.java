package animals;

public abstract class ZooMember {
	private String name, color, color2;
	private int legs;

	public ZooMember(String name, int legs, String color) {
		this.name = name;
		this.legs = legs;
		this.color = color;
	}
	public ZooMember(String name, int legs, String color, String color2) {
		this.name = name;
		this.legs = legs;
		this.color = color;
		this.color2 = color2;
	}
	public ZooMember(String name, String color) {
		this.name = name;
		this.color = color;
	}

	public ZooMember() {
		this.name = "Nie mam imienia :( ";
		this.legs = 0;
		this.color = "Jestem transparentny :(";
		this.color2 = "Na prawd� jestem transparentny :(";
		
	}
	public abstract void introduce();

	public int getLegs() {
		return legs;
	}

	public String getColor() {
		return color;
	}

	public String getName() {
		return name;
	}
	
	public String getColor2() {
		return color2;
	}

}
