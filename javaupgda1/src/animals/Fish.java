package animals;

public class Fish extends ZooMember implements Animal {
	public Fish(String name, String color) {
		super(name, color);
	}

	public Fish() {
	}

	public void introduce() {
		System.out.println("This is a " + this.getColor() + " fish, it's name is " + this.getName() + " and it has no legs.");
	}

	@Override
	public String makeNoise() {
		return "Blop blop blop.";
	}

}
