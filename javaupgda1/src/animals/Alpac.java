package animals;

public class Alpac extends ZooMember implements Animal {
	public Alpac(String name, int legs, String color) {
		super(name, legs, color);
	}

	public Alpac() {
	}

	public void introduce() {
		System.out.println("This is a " + this.getColor() + " alpac, it's name is " + this.getName() + " and it has "
				+ this.getLegs() + " legs.");
	}

	public String makeNoise(){
		return "Ik ik ik ik!";
	}
}
