package animals;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Save5 {
	public static void save5NoisesToFile(String filename, Animal animal) {
		try {
			PrintStream out = new PrintStream(filename);
			for (int i = 0; i<5 ; i++){
				out.println(animal.makeNoise());
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
