package animals;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class ZOO {
	public static void main(String[] args) {
		ZooMember giraffe = new Giraffe("Amelia", "yellow", "brown");
		ZooMember fish = new Fish("Goldie", "black");
		ZooMember llama = new Llama("Hector", 5, "gray");
		ZooMember alpac = new Alpac("Barnaba", 4, "white");
		ZooMember spider = new Spider("Spineo", 7, "pink");

		spider.introduce();
		alpac.introduce();
		fish.introduce();
		giraffe.introduce();
		llama.introduce();
	
		Animal[] animals = {new Fish(), new Alpac(), new Giraffe(), new Llama(), new Spider()};
		
		for (Animal animal : animals) {
			System.out.println(animal.makeNoise());
		}
		Save5.save5NoisesToFile("FishSoundTimesFive.txt", new Fish());
	}
}
