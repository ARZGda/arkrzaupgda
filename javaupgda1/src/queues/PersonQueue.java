package queues;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueue {

	public static void main(String[] args) {
		
		Queue<Person> persons = new PriorityQueue<>(new Comparator<Person>(){

			@Override
			public int compare(Person o1, Person o2) {

				return o1.getName().compareTo(o2.getName());
			}
		});
		
		persons.add(new Person("Andrzej",21));
		persons.add(new Person("Janusz",30));
		persons.add(new Person("Gra�yna",12));
		persons.add(new Person("Sebastian",49));
		
//		while(!persons.isEmpty()){
//		
//			System.out.println(persons.poll());
//		}
		while(!persons.isEmpty()){
			
		System.out.println(persons.poll());
		}
	}
}
