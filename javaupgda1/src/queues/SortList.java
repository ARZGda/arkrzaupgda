package queues;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortList {
	
	public static void main(String[] args) {
		
		List<Person> persons = new ArrayList<>();
		persons.add(new Person("Andrzej",21));
		persons.add(new Person("Janusz",30));
		persons.add(new Person("Gra�yna",12));
		persons.add(new Person("Sebastian",49));
		
		System.out.println("Przed sortowaniem");
		for (Person person : persons) {
			
			System.out.println(person);
		}
		
		Collections.sort(persons);
	
		System.out.println("Po sortowaniu");
		for (Person person : persons) {
			System.out.println(person);
		}
		
		System.out.println("Po przetasowaniu");
		Collections.shuffle(persons);
		for (Person person : persons) {
			System.out.println(person);
		}
	}
}
