package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class QuadraticEquation {
	private int a = 0;
	private int b = 0;
	private int c = 0;

	public double[] solve() throws ArithmeticException {
		double x1, x2, delta;
		while (a == 0 && b == 0 && c == 0) {
			a = getNumber();
			b = getNumber();
			c = getNumber();
		}

		delta = b * b - 4 * a * c;
		if (delta >= 0) {
			x1 = (Math.sqrt(delta) - b) / (2 * a);
			x2 = (-Math.sqrt(delta) - b) / (2 * a);
		} else {
			throw new ArithmeticException("Delta ujemna.");
		}
		return new double[] { x1, x2 };

	}

	private int getNumber() {
		int in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczb� ca�kowit�");
		try {
			in = sc.nextInt();
		} catch (InputMismatchException e) {
			in = 0;
		}
		return in;
	}

	public QuadraticEquation() {
		a = getNumber();
		b = getNumber();
		c = getNumber();
	}

	public QuadraticEquation(int a, int b, int c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

}
