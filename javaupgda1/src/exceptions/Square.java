package exceptions;

public class Square {

	public void square(int n) throws Exception {
	
		if (n >= 0) {

			System.out.println(Math.sqrt(n));

		} else {

			throw new IllegalArgumentException("Argument niepoprawny");

		}
	}
}
