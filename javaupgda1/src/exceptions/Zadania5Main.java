package exceptions;

public class Zadania5Main {
	
	public static void main(String[] args) {
		
		QuadraticEquation qe = new QuadraticEquation();
		
		double[] res = qe.solve();
		
		for (int i = 0; i < res.length; i++) {
			System.out.println("x"+ (i + 1) + " = "+ res[i]);
		}
		
	}
}
