package maps;

import java.util.HashMap;
import java.util.Map;

public class MapsExample {

	public static void main(String[] args) {
		Map<String, String> slownik = new HashMap<>();
		slownik.put("Kot", "Cat");
		slownik.put("Pies", "Dog");
		System.out.println("Klucze : ");
		for (String key : slownik.keySet()) {
			System.out.println(key);
		}
		
		System.out.println("Warto�ci :");
		for (String value : slownik.values()) {
			System.out.println(value);
		}
		
		System.out.println("Pary klucz - warto��");
		for(Map.Entry<String, String> pair : slownik.entrySet()){
			System.out.println(pair.getKey() + " -> "+pair.getValue());
		}
		for(Map.Entry<String, String> string : slownik.entrySet()){
			System.out.println(string);
		}
	}
}
