package maps;

public class UniversityAdd {
	public static void main(String[] args) {
		University university = new University();
		
		university.addStudent(10900, "Jan", "Kowalski");
		university.addStudent(10400, "Bartek", "Pieprzyński");
		university.addStudent(10200, "Joachim", "Artegor");
		university.addStudent(10300, "Herbert", "Angdon");
		
		university.showAll();
		System.out.println(university.getStudent(10400).getName());
		System.out.println(university.studentExists(10100));
		
	}
}
