package pl.org.javaup.exercises;

public class Main {
	
	public static void main(String[] args) {
		
//		Sentencer s = new Sentencer();
//		
//		String mySentence = s.readSentence("test.txt");
//		s.writeSentence("MySentence.txt", mySentence);
//		//System.out.println(s.readSentence("test.txt"));
		MoneyConverter conv = new MoneyConverter();
		TempConverter tcon = new TempConverter();
		
		System.out.println("Converting 120 USD to EUR: " + conv.convert(120, "USD", "EUR"));
		System.out.println("Converting 120 PLN to USD: " + conv.convert(120, "USD"));
		
		tcon.writeTemp(tcon.readTemp("tempC.txt"));
		
		LenghtChecker lcheck = new LenghtChecker();
		
		lcheck.make("words.txt", 8);
		
		AvgChecker avcheck = new AvgChecker();
		avcheck.process("marks.txt","marksOut.txt");
	}
}
