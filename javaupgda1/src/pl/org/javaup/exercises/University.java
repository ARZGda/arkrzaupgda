package pl.org.javaup.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class University {

	private final String path = "resources/";

	// -isStudentExists(index:int):boolean
	// +getStudent(index:int):Student
	// +putStudent(student:Student):void

	private boolean isStudentExists(int index) {
		String indexInRecord = "";
		
		indexInRecord += index;
		for (String record : readFile(path + "students.txt"))
		{
			if (record.contains(indexInRecord))
			{
				return true;
			} 
		}
		return false;
	}
	
//	public Student getStudent(int index) {
//		
//		if (isStudentExists(index)){
//			
//		}
//		
//	}

	private String[] readFile(String filename) {
		File f = new File(path + filename);
		String[] argTab = new String[countLines(filename)];
		int i = 0;
		try {
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				argTab[i++] = sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return argTab;
	}

	public int countLines(String filename) {
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return lines;
	}
}
