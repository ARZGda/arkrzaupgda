package pl.org.javaup.exercises;

import java.io.*;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class AvgChecker {

	private final String path = "resources/";
	private String filename;
	private int lines;

	public AvgChecker() {
	}

	public void process(String filename, String filename2) {
		this.setFilename(filename);

		File f = new File(path + filename);
		File f2 = new File(path + filename2);

		countLines(filename);
		String[][] tab = new String[lines][];
		int i = 0, j = 0;
		double avgFromAll = 0;
		int addCounter = 0;
		int addMainCounter = 0;
		double sum = 0;
		double avg = 0;
		double[] avgArray = new double[lines];
		System.out.println(lines);

		try {
			Scanner scn = new Scanner(f);

			for (i = 0; i < lines; i++) {
				sum = 0;
				System.out.println("Rekord nr " + (i + 1));
				String currLine = scn.nextLine();

				if (!currLine.equals("")) {
					tab[i] = currLine.split("\t");

					for (j = 1; j < tab[i].length; j++) {
						System.out.println("ocena : " + tab[i][j]);
						sum += Double.parseDouble(tab[i][j]);
						addCounter++;

					}

					avg = sum / addCounter;
					System.out.println("Suma ocen");
					System.out.println(sum);
					System.out.println("�rednia ocen: " + avg);
					addMainCounter++;
					avgFromAll += avg;
					avgArray[i] = avg;
					System.out.println("Suma �rednich");
					System.out.println(avgFromAll);
					addCounter = 0;
				}
			}
			scn.close();
			avgFromAll /= addMainCounter;
			System.out.println("�rednia �rednich :" + avgFromAll);

			FileOutputStream fos = new FileOutputStream(f2);
			PrintWriter pw = new PrintWriter(fos);
			for (i = 0; i < lines; i++) {
				/*
				
				Zawsze przed drukowaniem sprawdzi� czy dane w tablicach
				Nie s� r�wne 0!!!
								
				*/
				// System.out.println(avgArray[i] + " | " + avgFromAll);
				if (avgArray[i] > avgFromAll) {
					for (j = 0; j < tab[i].length; j++) {
						pw.print(tab[i][j].toString() + " ");
					}
					pw.println("");
				}

			}
			pw.close();

		}

		catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}

		catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}
	}

	public int countLines(String filename) {
		File f = new File(path + filename);
		lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return this.lines;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
