package pl.org.javaup.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConverter {

	private final String path = "resources/";

	// +toKelvin(temp:double):double
	// +toFahrenheit(temp:double):double
	// +readTemp(filename:String):double[]
	// +writeTemp(temp:double[]):void

	public double[] readTemp(String filename) {
		File f = new File(path + filename);
		double[] tempTab = new double[countLines(filename)];
		int i = 0;
		try {
			String currentLine = "";
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				tempTab[i] = Double.parseDouble(currentLine.replace(",", "."));
				i++;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		return tempTab;
	}
	
	public double toKelvin(double temp) {
		return temp + 273.15;
	}
	public double toFahrenheit(double temp) {
		return temp * 1.8 + 32;
	}
	
	public int countLines(String filename){
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		

		return lines;
	}
	
	public void writeTemp(double[] temp) {
		File f1 = new File("resources/f.txt");
		File f2 = new File("resources/k.txt");
		try {
		FileOutputStream fok = new FileOutputStream(f1);
		PrintWriter pwk = new PrintWriter(fok);
		FileOutputStream fof = new FileOutputStream(f2);
		PrintWriter pwf = new PrintWriter(fof);

		for (double d : temp) {
				pwf.println(toFahrenheit(d));
				pwk.println(toKelvin(d));

			}
		
		pwk.close();
		pwf.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	
		

	}
}
