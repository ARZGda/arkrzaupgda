package pl.org.javaup.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sentence {

	public String readSentence(String filename) {
		String sentence ="";
		File f = new File("resources/" + filename);
		
		try {
			Scanner sc = new Scanner(f);
			
			while (sc.hasNextLine()) {
				sentence += sc.nextLine() + " ";
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found.");
		}
		sentence = (sentence.substring(0,1).toUpperCase() + sentence.substring(1).toLowerCase()).trim();
		if (!sentence.endsWith(".")) {
			sentence +='.';
		}

		return sentence;
	}
}
