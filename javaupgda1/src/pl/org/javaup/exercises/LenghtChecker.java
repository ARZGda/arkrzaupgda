package pl.org.javaup.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LenghtChecker {

	private final String path = "resources/";
	
//	-isProperLength(String arg, int len):boolean
//	-readFile(String filename):String[]
//	-writeFile(String[] fileContent, int len):void
//	+make(String fileInput, int len):void
	
	private boolean isProperLength(String arg, int len) {
		if (arg.length()<=len)
		{
			return false;
		} else return true;
		
	}
	
	private String[] readFile(String filename) {
		File f = new File(path + filename);
		String[] argTab = new String[countLines(filename)];
		int i = 0;
		try {
			Scanner sc = new Scanner(f);
			
			while (sc.hasNextLine()) {
				argTab[i] = sc.nextLine();
				i++;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return argTab;
	}
	
	private void writeFile(String[] filecontent, int len) {
		File f = new File("resources/"+len+".txt");
		
		try {
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
			for (String string : filecontent) {
				if (isProperLength(string, len)){
					pw.println(string);
				}
				
			}
			pw.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public void make(String fileInput, int len) {
		String[] tab = readFile(fileInput);
		writeFile(tab, len);
	}
	
	public int countLines(String filename){
		File f = new File(path + filename);
		int lines = 0;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				lines++;
				sc.nextLine();
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	return lines;
	}
	
}
