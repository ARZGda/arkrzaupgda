package lists;

import java.util.List;

public class ListMath {

	public static int sum(List<Integer> numbers) {
		int suma =0;
		for (Integer i : numbers) {
			suma += i;
		}
		return suma;
	}
	
	public static int mult(List<Integer> numbers) {
		int multi = 1;
		for (int i : numbers) {
			multi *= i;
		}
		return multi;
	}
	
	public static double aver(List<Integer> numbers) {
		double average = 0;
		for (int i : numbers) {
			average += i;
		}
		average = average/numbers.size();
		return average;
	}
	
}
