package blocks;

public class Square extends Blocks {
	public Square(double a) {
		super((int) a);
	}
	
	public double countArea(double a) {
		return a*a;
	}
	public double countCircumference(double a) {
		return 4*a;
	}
	
}
