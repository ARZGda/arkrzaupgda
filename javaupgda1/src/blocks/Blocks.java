package blocks;

public abstract class Blocks {
	private int a, b, c;
	
	public Blocks(int a){
		this.a = a;
	}
	public Blocks(int a, int b, int c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	public int getA() {
		return a;
	}
	public int getB() {
		return b;
	}
	public int getC() {
		return c;
	}
	
	
}
