import java.util.HashMap;

/**
 * Created by Rzadki on 2017-06-29.
 */
public class Workplace {

    private HashMap<String, String> occupants = new HashMap<>();
    private HashMap<String,String> rooms = new HashMap<>();

    public void addOcucupantToRoom(String occupant, String room){
        occupants.put(room, occupant);
        rooms.put(occupant,room);
    }
    public void printAllRooms() {
        for (String room : occupants.keySet()){
            System.out.println("Pokój: " +room);
        }
    }
    public void printAllOccupants() {
        for (String occupant : occupants.values()) {
            System.out.println("Pracownik: " + occupant);
        }
    }
    public String getOccupant(String room) {
        return occupants.get(room);
    }
    public String getRoom(String occupant) {
        return rooms.get(occupant);
    }
}
