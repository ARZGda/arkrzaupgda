package StringUtil;

import java.util.Random;

public class StringUtil {

	private String str;

	public StringUtil(String str) {

		this.str = str;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil apend(String arg) {
		str = str + arg;
		return this;
	}

	public StringUtil letterSpacing() {
		String strSpaced = "";
		for (int i = 0; i < str.length() - 1; i++) {
			strSpaced += str.charAt(i) + " ";
		} 
		strSpaced += str.charAt(str.length() - 1);
		str = strSpaced;
		return this;
	}

	public StringUtil reverse() {
		String strReversed = "";
		for (int i = 0; i < str.length(); i++) {
			strReversed += str.charAt(str.length() - i);
		}
		str = strReversed;
		return this;
	}

	public StringUtil getAlphabet() {
		String alphabet = "";
		for (char c = 97; c <= 122; c++) {
			alphabet += c;
		}
		str = alphabet;

		return this;
	}

	public StringUtil getFirstLetter(){
		char firstLetter = str.trim().charAt(0);
		str = firstLetter + "";		
		return this;
	}

	public StringUtil limit(int n) {
		str = str.substring(0,n-1);
		return this;
	}

	public StringUtil insertAt(String insertion, int n) {
		str = str.substring(0,n) + insertion + str.substring(n);
		return this;
	}
	
	
	public StringUtil resetText() {
		str = "";
		return this;
	}
	
	public StringUtil swapLetters() {
		
		char firstLetter = str.trim().charAt(0);
		char lastLetter = str.trim().charAt(str.length()-1);
		str = lastLetter + str.substring(1, str.length()-1) + firstLetter;
		return this;
	}
	
	public StringUtil createSentence() {
		str = str.trim().toUpperCase().charAt(0)+str.substring(1);
		if (str.charAt(str.length()-1)=='.')
		{
			return this;
		} else {
			str += ".";
			return this;
		}
	}
	
	public StringUtil cut(int from, int to) {
		String[] cutted = str.split("");
		str = "";
		for (int i = from-1;i <=to-1; i++){
			str += cutted[i];
		}
		return this;		
	}
	
	public StringUtil pokemon() {
		Random r = new Random();
		String[] cutted = str.split("");
		String pokeOut = "";
		for (int i = r.nextInt(3);i < str.length(); i = i + r.nextInt(4)) {
			cutted[i] = cutted[i].toUpperCase();
		}
		for (int i = 0; i < str.length();i++) {
			pokeOut += cutted[i];
		}
		str = pokeOut;
		return this;
		}
	
	public StringUtil print() {
		System.out.println(str);
		return this;
	}

}
