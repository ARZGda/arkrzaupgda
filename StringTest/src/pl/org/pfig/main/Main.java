package pl.org.pfig.main;

import java.util.Random;

import StringUtil.HTMLExercise;

public class Main {

	public static void main(String[] args) {
		String myStr = "  przykladowy ciag znakow  ";

		if (myStr.equals("  przykladowy ciag znakow  ")) {

			System.out.println("Ci�gi s� takie same.");

		}
		if (myStr.equalsIgnoreCase("  PRZYKLADOWY ciag znaKOW  ")) {

			System.out.println("Ci�gi s� takie same, nie sprawdza�em wielko�ci liter");
		}
		System.out.println("Dlugosc myStr " + myStr.length());

		System.out.println(myStr.substring(14));

		System.out.println(myStr.substring(3, myStr.length() - 3));

		System.out.println(myStr);

		System.out.println(myStr.trim());

		System.out.println(myStr.charAt(5));

		String alphabet = "";
		for (char c = 97; c <= 122; c++) {
			alphabet += c;
		}

		System.out.println(alphabet);

		System.out.println(myStr.replace("ciag", "lancuch"));

		System.out.println(myStr.concat(alphabet));
		if (myStr.contains("klad")) {
			System.out.println("Slowo klad zawiera sie w " + myStr);

		}
		if (alphabet.endsWith("z")) {
			System.out.println("Alfabet ko�czy si� na literze Z");
		}
		if (alphabet.startsWith("a")) {
			System.out.println("Alfabet zaczyna si� liter� A");
		}

		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));

		String simpleStr = "pawel poszedl do lasu";
		String[] arrOfStr = simpleStr.split(" ");
		for (String s : arrOfStr) {
			System.out.println("\t" + s);
		}

		
		Random r = new Random();
		System.out.println(r.nextInt());
		
		int a=10;
		int b= 25;
		System.out.println(a+r.nextInt(b-a));
		char c = (char) (97 + r.nextInt(26));
		System.out.println(c+"");
		
		HTMLExercise he = new HTMLExercise("To jest m�j tekst");
		
		he.print().strong().print().p().print();
		
	}
}
