package oop;

public class OsobaMain {

	public static void main(String[] args) {

		Osoba jan = new Osoba("Jan",23);
		Osoba andrzej = new Osoba("Andrzej",47);
		
		jan.przedstawSie();
		jan.setImie("Artur");
		jan.przedstawSie();
		andrzej.przedstawSie();
		
		
		new Osoba();
	}

}
