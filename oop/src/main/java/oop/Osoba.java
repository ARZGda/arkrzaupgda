package oop;

public class Osoba {
	
	//dane s� prywatne
	private String imie;
	private int wiek;
	
/*	public Osoba(){
		System.out.println("Konstruktor uruchomiony"); //komunikuje za ka�dym razem gdy u�ywam metody Osoba() - tej konkretnej, czyli bez parametr�w.
	}
*/
	//Poni�ej znowu przeci��am konstruktory
	//konstruktor bez parametr�w
	public Osoba(){
		System.out.println("Konstruktor bez parametr�w.");
	}
	
	//konstruktor z parametrami
	
	public Osoba(String imie, int wiek)				//przypisuj� dane do 'Osoba' bez konieczno�ci u�ywania jan.imie etc.
	{
		this.imie = imie;
		this.wiek = wiek;
	}
	
	//poni�ej metody zmiany imienia i wieku, wygenerowane automatycznie - Source>Generate setters and getters
	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public int getWiek() {
		return wiek;
	}

	public void setWiek(int wiek) {
		this.wiek = wiek;
	}

	public void przedstawSie()
	{
		System.out.println(imie + " ("+wiek+")");
	}
}
