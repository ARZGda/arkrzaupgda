package QuadraticEquation;

public class QuadraticEquation {

	public QuadraticEquation(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	private double a;
	private double b;
	private double c;

	public double delta()
	{
		
		return b*b-4*a*c;
	}
	public double wzx1()
	{
		return (-b+Math.sqrt(delta()))/(2*a);
	}
	public double wzx2()
	{
		return (-b-Math.sqrt(delta()))/(2*a);
	}
}
