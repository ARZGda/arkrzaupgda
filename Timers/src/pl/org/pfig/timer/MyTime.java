package pl.org.pfig.timer;

public class MyTime {

	private int hour = 0;
	private int minute = 0;
	private int second = 0;

	public MyTime() {

	}

	public MyTime(int hour, int minute, int second) {
		setHour(hour+24);
		setMinute(minute+60);
		setSecond(second+60);
	}

	public String leadNumber(int num) {
		if (num < 10) {
			return "0" + num;
		} else {
			return "" + num;
		}
	}

	@Override
	public String toString() {
		return leadNumber(this.hour) + ":" + leadNumber(this.minute) + ":" + leadNumber(this.second);
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {

			this.hour = hour % 24;

	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {

			this.minute = minute % 60;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {

			this.second = second % 60;
	}

	public void setTime(int hour, int minute, int second) {
		setHour(hour);
		setMinute(minute);
		setSecond(second);
	}

	public MyTime nextSecond() {
		int newHour = hour, newMinute = minute, newSecond = second+1;
		if (newSecond % 60 == 0) {
			newSecond = 0;
			newMinute++;
			if (newMinute % 60 == 0){
				newMinute = 0;
				newHour++;
			}
		}
		
		return new MyTime(newHour,newMinute,newSecond);
	}

	public MyTime nextMinute() {
		int newHour = hour, newMinute = minute+1;
		if (newMinute % 60 == 0) {
			newMinute = 0;
			newHour = hour+1;
		}
		return new MyTime(newHour,newMinute,second);

	}

	public MyTime nextHour(){
		
		int newHour = hour+1;
		return new MyTime(newHour,minute,second);
	}	
	
	public MyTime pastSecond() {
		int newHour = hour, newMinute = minute, newSecond = second-1;
		if ((newSecond+60) % 60 == 59) {
			newMinute = newMinute-1;
			if ((newMinute+60) % 60 == 59){
				newHour = newHour-1;
			}
		}
	
		return new MyTime(newHour,newMinute,newSecond);
	}

	public MyTime pastMinute() {
		int newHour = hour, newMinute = minute-1;
		if ((newMinute+60) % 60 == 59) {
			newHour = newHour-1;
		}
		return new MyTime(newHour,newMinute,second);

	}

	public MyTime pastHour(){
		
		int newHour = hour-1;
		return new MyTime(newHour,minute,second);
	}
}
