package pl.org.pfig.main;
import pl.org.pfig.timer.*;
public class Main {

	public static void main(String[] args) {
		MyTime t1 = new MyTime(23,59,59);
		MyTime t2 = t1.nextHour();
		MyTime t3 = t1.pastHour();
		MyTime t4 = t1.nextMinute();
		MyTime t5 = t1.pastMinute();
		MyTime t6 = t1.nextSecond();
		MyTime t7 = t1.pastSecond();
		System.out.println("M�j czas : "+t1);
		System.out.println(t1+" +1h "+t2);
		System.out.println(t1+" -1h "+t3);
		System.out.println(t1+" +1min "+t4);
		System.out.println(t1+" -1min "+t5);
		System.out.println(t1+" +1sec "+t6);
		System.out.println(t1+" -1sec "+t7);
	}

}
