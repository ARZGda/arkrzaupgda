var app = angular.module('cookApp', ['ngRoute']);
var url = 'http://localhost:8088/';

app.config(function ($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
		.when('/login', {
			templateUrl: path + 'user/login.html'
		})
		.when('/register', {
			templateUrl: path + 'user/register.html'
		})
        .when('/add', {
            templateUrl: path + 'recipies/add.html',
            controller: 'addController'
        })
        .when('/editRecipes/:id', {
            templateUrl: path + 'recipies/edit.html',
            controller: 'editController'
        })
	    .when('/editCountries/:id', {
            templateUrl: path + 'countries/edit.html',
            controller: 'editCountryController'
        })
	    .when('/editCategories/:id', {
            templateUrl: path + 'categories/edit.html',
            controller: 'editCategoryController'
        })
        .when('/deleteRecipes/:id', {
            templateUrl: path + 'recipies/delete.html',
            controller: 'deleteController'
        })
		.when('/deleteCountries/:id', {
            templateUrl: path + 'countries/delete.html',
            controller: 'deleteCountryController'
        })
		.when('/deleteCategories/:id', {
            templateUrl: path + 'categories/delete.html',
            controller: 'deleteCategoryController'
        })
        .when('/showCategories', {
            templateUrl: path + 'categories/categories.html',
            controller: 'categoriesController'
        })
		.when('/showCountries', {
			templateUrl: path + 'countries/countries.html',
			controller: 'countriesController'
		})
        .when('/showCategories/:id', {
            templateUrl: path + 'categories/category.html',
            controller: 'categoryController'
        })
        .when('/showCountries/:id', {
            templateUrl: path + 'countries/country.html',
            controller: 'countryController'
        })
        .when('/showRecipes', {
            templateUrl: path + 'recipes/recipes.html',
            controller: 'recipesController'
        })
		.when('/showRecipies/:id', {
			templateUrl: path + 'recipes/recipe.html',
			controller: 'recipeController'
	    });
});

app.controller('addController', function ($scope, $http) {
    
    $http({
        url: url + 'recipes/add',
        dataType: 'json'
    }).then(function (success) {
        $scope.species = success.data;
    }, function (error) {
        console.error(error);
    });
    
    $scope.add = function () {
        $http({
            url: url + 'recipies/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.name,
                country: $scope.country,
                category: $scope.category,
				products: $scope.products,
                description: $scope.description
            }
        }).then(function (success) {
            console.log(success);
            $scope.message = "Receptura dodana poprawnie.";
        }, function (error) {
            console.error(error);
        });
    };
});

app.controller('recipiesController', function ($scope, $http) {
    $http({
        url: url + 'recipies/show',
        dataType: 'json'
    }).then(function (success) {
        $scope.recipies = success.data;
    }, function (error) {
        console.error(error);
    });
});

app.controller('recipeController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'recipies/show/' + id,
        dataType: 'json'
    }).then(function (success) {
        $scope.recipe = success.data;
    }, function (error) {
        console.error(error);
    });
});

app.controller('countriesController', function ($scope, $http) {
    
    $http({
        url: url + 'countries/countries'
    }).then(function (success) {
        $scope.species = success.data;
    }, function (error) {
        console.error(error);
    });
    
    
    $scope.add = function () {
        $http({
            url: url + 'countries/add',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.description
            }
        }).then(function (success) {
            if (success.data.id > 0) {
                $scope.countries.push(success.data);
                $scope.message = "Kraj został dodany do menu.";
            } else {
                $scope.message = "Wystąpił błąd podczas dodawania kraju.";
            }
        }, function (error) {
            console.error(error);
        });
    };
});

app.controller('categoriesController', function ($scope, $http) {
    
    $http({
        url: url + 'categories/categories'
    }).then(function (success) {
        $scope.species = success.data;
    }, function (error) {
        console.error(error);
    });
    
    
    $scope.add = function () {
        $http({
            url: url + 'categories/add',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.description
            }
        }).then(function (success) {
            if (success.data.id > 0) {
                $scope.categories.push(success.data);
                $scope.message = "Kategoria została dodana do menu.";
            } else {
                $scope.message = "Wystąpił błąd podczas dodawania kategorii.";
            }
        }, function (error) {
            console.error(error);
        });
    };
});

app.controller('countryController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'country/show/' + id,
        dataType: 'json'
    }).then(function (success) {
        $scope.country = success.data;
    }, function (error) {
        console.error(error);
    });
});

app.controller('categoryController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'category/show/' + id,
        dataType: 'json'
    }).then(function (success) {
        $scope.country = success.data;
    }, function (error) {
        console.error(error);
    });
});

app.controller('deleteController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'recipies/delete/' + id,
        dataType: 'json'
    }).then(function (success) {
        $scope.message = "Usunięto poprawnie.";
    }, function (error) {
        console.error (error);
    });
});

app.controller('deleteCountryController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'countries/delete/' + id,
        dataType: 'json'
    }).then(function (success) {
        $scope.message = "Usunięto poprawnie.";
    }, function (error) {
        console.error (error);
    });
});

app.controller('deleteCategoryController', function ($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'categories/delete/' + id,
        dataType: 'json'
    }).then(function (success) {
        $scope.message = "Usunięto poprawnie.";
    }, function (error) {
        console.error(error);
    });
});

app.controller('editController', function ($scope, $http, $routeParams) {

    $http({
        url: url + 'recipies/show',
        dataType: 'json'
    }).then(function (success) {
        $scope.recipies = success.data;
    }, function (error) {
        console.error (error);
    });
    
    $http({
        url: url + 'recipies/show/' + $routeParams.id,
        dataType: 'json'
    }).then(function (success) {
        $scope.recipe = success.data;
    }, function (error) {
        console.error(error);
    });
    
    $scope.edit = function () {
        $http({
            url: url + 'recipies/edit',
            dataType: 'json',
            params: {
                id: $routeParams.id,
                name: $scope.recipe.name,
				country: $scope.recipe.country,
				category: $scope.recipe.category,
				description: $scope.recipe.description
            }
        }).then(function (success) {
            $scope.recipe = success.data;
        }, function (error) {
            console.error(error);
        });
    };
});

app.controller('editCountryController', function ($scope, $http, $routeParams) {

    $http({
        url: url + 'countries/show',
        dataType: 'json'
    }).then(function (success) {
        $scope.countries = success.data;
    }, function (error) {
        console.error (error);
    });
    
    $http({
        url: url + 'countries/show/' + $routeParams.id,
        dataType: 'json'
    }).then(function (success) {
        $scope.country = success.data;
    }, function (error) {
        console.error (error);
    });
    
    $scope.edit = function() {
        $http({
            url: url + 'countries/edit',
            dataType: 'json',
            params: {
                id: $routeParams.id,
                name: $scope.country.name,
				description: $scope.country.description
            }
        }).then(function (success) {
            $scope.country = success.data;
        }, function (error) {
            console.error(error);
        });
    };
});

app.controller('editCountryController', function ($scope, $http, $routeParams) {

    $http({
        url: url + 'categories/show',
        dataType: 'json'
    }).then(function (success) {
        $scope.countries = success.data;
    }, function (error) {
        console.error(error);
    });
    
    $http({
        url: url + 'categories/show/' + $routeParams.id,
        dataType: 'json'
    }).then(function (success) {
        $scope.category = success.data;
    }, function (error) {
        console.error (error);
    });
    
    $scope.edit = function () {
        $http({
            url: url + 'categories/edit',
            dataType: 'json',
            params: {
                id: $routeParams.id,
                name: $scope.category.name,
				description: $scope.category.description
            }
        }).then(function (success) {
            $scope.category = success.data;
        }, function (error) {
            console.error(error);
        });
    };
});