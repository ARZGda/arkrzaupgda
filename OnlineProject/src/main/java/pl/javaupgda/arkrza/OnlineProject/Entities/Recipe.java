package pl.javaupgda.arkrza.OnlineProject.Entities;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private String products;
    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)

    private Country country;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)

    private Category category;

    public Recipe() {}

    public Recipe(long id, String name,String products, String description) {
        this.id = id;
        this.name = name;
        this.products = products;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
