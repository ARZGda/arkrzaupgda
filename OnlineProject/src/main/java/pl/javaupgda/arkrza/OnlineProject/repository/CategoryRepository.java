package pl.javaupgda.arkrza.OnlineProject.repository;

import org.springframework.stereotype.Repository;
import pl.javaupgda.arkrza.OnlineProject.Entities.Category;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
}