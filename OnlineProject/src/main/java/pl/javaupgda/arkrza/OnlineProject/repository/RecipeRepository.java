package pl.javaupgda.arkrza.OnlineProject.repository;

import org.springframework.stereotype.Repository;
import pl.javaupgda.arkrza.OnlineProject.Entities.Recipe;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
