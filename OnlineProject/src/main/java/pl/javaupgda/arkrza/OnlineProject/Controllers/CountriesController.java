package pl.javaupgda.arkrza.OnlineProject.Controllers;

import pl.javaupgda.arkrza.OnlineProject.Entities.Category;
import pl.javaupgda.arkrza.OnlineProject.Entities.Country;
import pl.javaupgda.arkrza.OnlineProject.repository.CategoryRepository;
import pl.javaupgda.arkrza.OnlineProject.repository.CountryRepository;
import pl.javaupgda.arkrza.OnlineProject.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/countries")
public class CountriesController {

    @Autowired
    private CountryRepository countryRepository;

   @RequestMapping("/addCountries")
    public Country add(@RequestParam(name = "name") String name,
                       @RequestParam(name = "description") String description) {
        Country country = new Country();
        country.setName(name);
        country.setDescription(description);
        return countryRepository.save(country);
    }

    @RequestMapping("/showCountries")
    public List<Country> showAll() {
        return (List<Country>) countryRepository.findAll();
    }

    @RequestMapping("/showCountries/{id}")
    public Country showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return countryRepository.findOne(myId);
    }
}