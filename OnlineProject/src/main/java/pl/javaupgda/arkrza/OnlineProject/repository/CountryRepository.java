package pl.javaupgda.arkrza.OnlineProject.repository;

import org.springframework.stereotype.Repository;
import pl.javaupgda.arkrza.OnlineProject.Entities.Country;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {
}
