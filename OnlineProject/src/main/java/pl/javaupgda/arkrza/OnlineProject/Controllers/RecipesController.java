package pl.javaupgda.arkrza.OnlineProject.Controllers;

import pl.javaupgda.arkrza.OnlineProject.Entities.Category;
import pl.javaupgda.arkrza.OnlineProject.Entities.Country;
import pl.javaupgda.arkrza.OnlineProject.Entities.Recipe;
import pl.javaupgda.arkrza.OnlineProject.repository.CategoryRepository;
import pl.javaupgda.arkrza.OnlineProject.repository.CountryRepository;
import pl.javaupgda.arkrza.OnlineProject.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.*;


@CrossOrigin
@RestController
@RequestMapping("/animals")
public class RecipesController {

    @Autowired
    private RecipeRepository recipeRepository;

    @RequestMapping("/")
    public String recipe() {
        return "";
    }

    @RequestMapping("/show")
    public List<Recipe> listRecipes() {
        return (List<Recipe>) recipeRepository.findAll();
    }

    @RequestMapping("/addRecipes")
    public Recipe addRecipe(@RequestParam(value = "name") String name,
                            @RequestParam(value = "description") String description,
                            @RequestParam(value = "products") String products,
                            @RequestParam(value = "country") Country country,
                            @RequestParam(value = "category") Category category)
    {
        Recipe recipe = new Recipe();
        recipe.setName(name);
        recipe.setDescription(description);
        recipe.setProducts(products);
        recipe.setCountry(country);
        recipe.setCategory(category);
        return recipeRepository.save(recipe);
    }

    @RequestMapping("/showRecipies/{id}")
    public Recipe showRecipeByID(@PathVariable("id") String id) {
        return recipeRepository.findOne(Long.valueOf(id));
    }

}