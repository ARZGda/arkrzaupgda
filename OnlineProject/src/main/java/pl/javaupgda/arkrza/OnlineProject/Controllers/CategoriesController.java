package pl.javaupgda.arkrza.OnlineProject.Controllers;

import pl.javaupgda.arkrza.OnlineProject.Entities.*;
import pl.javaupgda.arkrza.OnlineProject.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/categories")
public class CategoriesController {

    @Autowired
    private CategoryRepository categoryRepository;

    @RequestMapping("/addCategories")
    public Category add(@RequestParam(name = "name") String name,
                        @RequestParam(name = "description") String description) {
        Category category = new Category();
        category.setName(name);
        category.setDescription(description);
        return categoryRepository.save(category);
    }

    @RequestMapping("/showCategories")
    public List<Category> showAll() {
        return (List<Category>) categoryRepository.findAll();
    }

    @RequestMapping("/showCategories/{id}")
    public Category showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return categoryRepository.findOne(myId);
    }
}