package blocks;

public class Triangle extends Blocks {
	public Triangle (double a, double b, double c){
		super((int) a, (int) b, (int) c);
	}
	
	public double countArea(double a, double b, double c) {
		double area = 0;
		double halfCircumference = 0;
		halfCircumference = (a+b+c)/2;
		area = Math.sqrt(halfCircumference*(halfCircumference-a)*(halfCircumference-b)*(halfCircumference-c));
		return area;
	}
	public double countCircumference(double a, double b, double c) {
		return a+b+c;
	}
}
