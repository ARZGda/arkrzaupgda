package repoName;

public class RepoName {

	private String name;
	private String surname;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public static String getRepoName(String name, String surname) {
		int cut = 3;
		
		if (name.toLowerCase().charAt(2) == surname.toLowerCase().charAt(0)) {

			cut++;
		
		} 
		return name.substring(0, 1).toUpperCase() + name.substring(1, cut).toLowerCase()
				+ surname.substring(0, 1).toUpperCase() + surname.substring(1, cut).toLowerCase() + "UPgda";
	}
		
	public String getRepoName() {
		return RepoName.getRepoName(name, surname);
	}
}
