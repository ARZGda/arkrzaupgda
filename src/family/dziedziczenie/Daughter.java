package family.dziedziczenie;

public class Daughter extends FamilyMember {

	public Daughter(String name) {
		super(name);
	}
	public void introduce (){
		System.out.println("I'm a daughter. My namie is "+this.getName());
	}
}
