package family.dziedziczenie;

public class Son extends FamilyMember {
	
	public Son(String name) {
		super(name);
	}
	public void introduce (){
		System.out.println("I'm a son. My namie is "+this.getName());
	}
}
