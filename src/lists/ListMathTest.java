package lists;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListMathTest {

	@Test
	public void sumTest() {
		List<Integer> numbers = new ArrayList<>();
		numbers.add(4);
		numbers.add(5);
		numbers.add(8);
		
		assert ListMath.sum(numbers) == 17;
	}
	
	@Test
	public void multTest() {
		List<Integer> numbers = new ArrayList<>();
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		
		assert ListMath.mult(numbers) == 24;
	}
	
	@Test
	public void averTest() {
		List<Integer> numbers = new ArrayList<>();
		numbers.add(2);
		numbers.add(3);
		numbers.add(4);
		
		assert ListMath.aver(numbers) == 3;
	}
}
