package lists;

import java.util.ArrayList;
import java.util.List;

public class ListExamples {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<Integer>();
		
		numbers.add(5);
		numbers.add(3);
		numbers.add(1);
		numbers.add(6);
		numbers.add(5);
		numbers.add(4);
		
		System.out.println("Ca�a lista :");
		for (int i : numbers) {
			System.out.println(i);
		}
		
		System.out.println("Element nr 2 :");
		System.out.println(numbers.get(2));
		
		numbers.remove((Integer)5);
		
		System.out.println("Lista po usuni�ciu pierwszej pi�tki :");
		for (int i : numbers) {
			System.out.println(i);
		}
		
		numbers.add(1, 100);
		
		System.out.println("Lista po wstawieniu rekordu nr 1 r�wnego 100: ");
		for (int i : numbers) {
			System.out.println(i);
		}
		
		System.out.println("Rozmiar listy :");
		System.out.println(numbers.size());
	}
	
}
