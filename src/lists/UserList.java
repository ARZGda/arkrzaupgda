package lists;

import java.util.ArrayList;
import java.util.List;

public class UserList {
	public static void main(String[] args) {
		
		List<User> users = new ArrayList<>();
			users.add(new User("admin","admin"));
			users.add(new User("ziomek","jpstopro"));
	
			for (User user : users) {
				System.out.println(user.printUserAndPassword());
			}
	}	
}
