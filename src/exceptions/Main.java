package exceptions;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		ExceptionSimpleClass esc = new ExceptionSimpleClass();

		// try {
		// esc.make(55);
		// } catch (IllegalArgumentException e) {
		//
		// System.out.println("[ ERROR ] " + e.getMessage());
		//
		// } catch (Exception e) {
		//
		// System.out.println(e.getMessage());
		//
		// } finally {
		//
		// System.out.println("[ END ]");
		//
		// }

		int[] array = new int[10];

		Scanner scanint = new Scanner(System.in);

		for (int i : array) {
			System.out.println("Podaj liczb� " + i);
			array[i] = scanint.nextInt();
		}
		scanint.close();

		try {

			System.out.println(array[10]);

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Brak elementu pod indeksem " + e.getMessage());
		}
	}
}
