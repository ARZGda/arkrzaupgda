package exceptions;

public class Divide {

	public static double divide(int a, int b) throws Exception {
		if (b == 0){
			throw new IllegalArgumentException("Nie mo�na dzieli� przez zero");
		} else {
			return a/b; 
		}
	}

	public static double divide(double a,double b){
		if (b == 0){
			throw new IllegalArgumentException("Nie mo�na dzieli� przez zero");
		} else {
			return a/b; 
		}
	}
	
}
