package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	public double readDouble() {
		double in =0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj double: ");
		try {
			in = sc.nextDouble();
		} catch (IllegalArgumentException e) {
		} catch (InputMismatchException e) {
		}
		sc.close();
		return in;
	}
	
	public int readInt() {
		int in = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj int: ");
		try {
			in = sc.nextInt();
 		} catch (IllegalArgumentException e) {
 		} catch (InputMismatchException e) {
		}
		sc.close();
		return in;
	}
	
	public String readStream() {
		String in = "";
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj Stream: ");
		try {
			in = sc.nextLine();
		} catch (IllegalArgumentException e) {
		} catch (InputMismatchException e) {
		}
		sc.close();
		return in;
		}
}
