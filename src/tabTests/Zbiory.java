package tabTests;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Zbiory {

	public static void main(String[] args) {
		int[] tab = {5000,2000,1000,3000,4000,2000,2000,3000,4000};
		
		Set<Integer> set = new HashSet<>();
		
		for (int i : tab) {
			set.add(i);
		}
		
		System.out.println("Rozmiar zbioru : " + set.size());
		System.out.println("Zbi�r :");
		for (int i : set) {
			System.out.println(i);
		}
		set.remove(1000);
		set.remove(2000);
		
		System.out.println("Zbi�r :");
		for (int i : set) {
			System.out.println(i);
		}
		
		System.out.println("Iterable");
		Iterable <Integer> iterable = set;
		for (int i : iterable) {
			
			System.out.println(i);
		}
		
		
		Iterator<Integer> iterator = set.iterator();
		System.out.println("Pojedynczy element");
		System.out.println(iterator.next());
		System.out.println("Nast�pny element");
		System.out.println(iterator.next());
		
		Iterator<Integer> it = set.iterator();
		while (it.hasNext()){
			System.out.println(it.next());
		}
		
		
		
	}

}
