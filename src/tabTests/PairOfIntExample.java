package tabTests;

import java.util.HashSet;
import java.util.Set;

public class PairOfIntExample {
	public static void main(String[] args) {
		
		Set<PairOfInt> pairs = new HashSet<>();
		pairs.add(new PairOfInt(1, 2));
		pairs.add(new PairOfInt(2, 1));
		pairs.add(new PairOfInt(1, 1));
		pairs.add(new PairOfInt(1, 2));
		
		for (PairOfInt pairOfInt : pairs) {
			System.out.println(pairOfInt);
		}
		
		
		
	}
}
