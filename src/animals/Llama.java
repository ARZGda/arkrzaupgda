package animals;

public class Llama extends ZooMember implements Animal {
	public Llama(String name, int legs, String color) {
		super(name, legs, color);
	}

	public Llama() {
	}

	public void introduce() {
		System.out.println("This is a " + this.getColor() + " llama, it's name is " + this.getName() + " and it has "
				+ this.getLegs() + " legs.");
	}

	@Override
	public String makeNoise() {
		return "Oik oik";
	}


}
