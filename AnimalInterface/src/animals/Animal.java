package animals;

import java.util.*;

public class Animal {

	private List<String> animals = new LinkedList<>();
	
	public Animal() {}
	
	public void addAnimal(String animal, AnimalInterface ai) {
		String[] animals = ai.make(animal);
		for(String a : animals) {
			this.animals.add(a);
		}
	}
	
	public String getAnimal(int index) {
		return (animals.size() > index) ?animals.get(index) : "";
	}
	
	public boolean containsAnimal(String animal, AnimalInterface ai) {
		String[] exists = ai.make(animal);
		for (String a : exists) {
			if(!animals.contains(a)){
				return false;
			}
		}
		return true;
	}
}
