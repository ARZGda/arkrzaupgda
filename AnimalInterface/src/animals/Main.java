package animals;

public class Main {

	public static void main(String[] args) {
		
		Animal a = new Animal();
		
		a.addAnimal("Slon", (anims) -> (anims.split(" ")));
		a.addAnimal("pies|kot|zaba", (anims) -> (anims.split("\\|")));
		
		a.addAnimal("aligator_waz", (anims) -> (anims.split("_")));
		
		a.addAnimal("katp", (anims) -> {
			String[]ret = new String[1];
			ret[0] = "";

			for(int i = anims.length() -1 ; i>= 0; i--) {
				ret[0] += anims.charAt(i) + "";
			}
			return ret;
		});
		System.out.println(a.containsAnimal("kot|zaba", (anims) -> (anims.split("\\|"))));
	}
}
