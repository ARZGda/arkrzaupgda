package pl.org.pfig;

import java.util.NoSuchElementException;

/**
 * + * Created by Adrian on 2017-06-23.
 * +
 */
public class SimpleArrayList implements SimpleList {
    private int number;
    private int[] data = new int[100];

    public void add(int value) {
        if (number < data.length) {
            data[number] = value;
            number++;
        } else {
            throw new RuntimeException("Za mało miejsca w liście.");
        }
    }

    public int get(int index) {
        if (index < number) {
            return data[index];
        } else {
            throw new NoSuchElementException();
        }
    }

    public void add(int value, int index) {
        if (number < data.length) {
            if (index < number) {
                for (int i = number; i > index; i--) {
                    data[i] = data[i - 1];
                }
                data[index] = value;
            } else {
                arrayToSmall();
            }
            number++;
        } else throw new IllegalArgumentException();
    }

    public boolean contain(int value) {
        for (int i : data) {
            if (i == value) {
                return true;
            }
        }
        return false;
    }

    public void remove(int index) {
        if (index < number && index >= 0) {
            for (int i = index; i < number-1; i++) {
                data[i] = data[i + 1];
            }
            number--;
        } else {
            throw new IllegalArgumentException("Zły indeks");
        }
    }

    public void removeValue(int value) {


    }

    public int size() {
        return number;
    }

    private void arrayToSmall() {
        throw new IndexOutOfBoundsException("Array too small.");
    }
}