package pl.org.pfig;

import java.security.interfaces.ECKey;

/**
 * Created by RENT on 2017-06-26.
 */
public class SimpleLinkedList implements SimpleList {
    private Element first;
    private Element last;
    private int number;

    @Override
    public void add(int value) {
        Element element = new Element();
        element.value = value;
        if (number == 0) {
            first = element;
        } else {
            element.prev = last;
            last.next = element;
        }
        last = element;
        number++;
    }

    @Override
    public void add(int value, int index) {
        int i = 0;
        if (index == number) {
            add(value);
        } else if (index == 0) {
            Element element = new Element();
            element.value = value;
            element.next = first;
            first = element;

        } else if (index > 0 && index < number) {
            Element element = first;
            while (i != index) {
                element = element.next;
                i++;
            }
            Element newElement = new Element();
            newElement.value = value;
            Element previous = element.prev;

            previous.next = newElement;
            element.prev = newElement;
            newElement.prev = previous;
            newElement.next = element;
        } else {
            throw new IndexOutOfBoundsException();
        }

    }

    @Override
    public int get(int index) {
        Element element = first;
        int i = 0;
        if (index < number) {
            element = element.next;
            i++;
        } else if (index == number) {
            return element.value;
        } else if (index > number) {
            throw new IndexOutOfBoundsException();
        }
        return 0;
    }

    @Override
    public boolean contain(int value) {
        Element element = first;
        while (element != null) {
            if (element.value == value) {
                return true;
            }
            element = element.next;
        }
        return false;
    }

    @Override
    public void remove(int index) {
        Element element = first;
        int i = 0;
        if (index == 0) {
            if (number == 1) {
                first = null;
                last = null;
            } else {
                first = first.next;
                first.prev = null;
            }
        }
        if (index < number) {
            while (i != index) {
                element = element.next;
                i++;
            }
            Element previous = element;
            element = element.next;
            previous.next = element.next;
            element.next.prev = previous;

        } else if (index == number - 1) {
            last = last.prev;
            last.next = null;
        } else {
            throw new IndexOutOfBoundsException();
        }
        number--;
    }

    @Override
    public void removeValue(int value) {
        Element element = first;
        int index = 0;
        for (int i =0;i<number;i++) {
            element = element.next;
            if (element.value == value) {
                remove(index);
            }
            index++;
        }

    }

    @Override
    public int size() {
        return number;
    }

    private static class Element {
        int value;
        Element next;
        Element prev;
    }
}
