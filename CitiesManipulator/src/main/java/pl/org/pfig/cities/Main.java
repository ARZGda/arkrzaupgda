package pl.org.pfig.cities;

import pl.org.pfig.cities.connector.DatabaseConnector;

import java.sql.*;

/**
 * Created by RENT on 2017-07-06.
 */
public class Main {
    public static void main(String[] args) {

        String url = "jdbc:mysql://localhost:3306/rental";
        String user = "root";
        String pass = "";

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("INSERT INTO `cityTab` VALUES(NULL, 'Radom');");
            System.out.println("Dodałem Radom do Bazy Danych");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            String query = "UPDATE `cityTab` SET `city` = ? WHERE `city` = ?";
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1, "Trójmiasto");
            ps.setString(2, "Gdańsk");
            int countRows = ps.executeUpdate();
            System.out.println("Nadpisałem "+countRows+" wiersz(y).");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("UPDATE `cityTab` SET `city`='Wąchock' WHERE `city` LIKE 'Radom';");
            System.out.println("O nie! Radom zmienił się w Wąchock!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        try {
            Connection c = DatabaseConnector.getInstance().getConnection();
            Statement s = c.createStatement();
            s.execute("DELETE FROM `cityTab` WHERE `city`='Wąchock';");
            System.out.println("Usunąłem Wąchock z Bazy Danych");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        try {
            Connection connection = DatabaseConnector.getInstance().getConnection();
            String query = "SELECT * FROM cityTab";
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery(query);
            while(rs.next()) {
                System.out.println(rs.getInt("id")+". "+rs.getString("city"));

            }
            rs.close();
            s.close();
            connection.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
