import model.Student;
import resolver.student.StudentResolver;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by RENT on 2017-07-06.
 */
public class Main {
    public static void main(String[] args) {

        Scanner intSc = new Scanner(System.in);
        Scanner strSc = new Scanner(System.in);
        int usrChoice = 0;

        while (usrChoice != 6) {

            UserMenu();

            usrChoice = intSc.nextInt();
            if (usrChoice == 1) {
                /* Pokaż rekordy. */
                try {
                    StudentResolver sr = new StudentResolver();
                    List<Student> printRecords = sr.get();

                    System.out.println("rozmiar listy: " + printRecords.size());
                    for (Student s : printRecords) {
                        System.out.println(s.toString());
                    }
                } catch (NullPointerException e) {
                    System.out.println("Lista rekordów jest pusta.");
                }
            }

            if (usrChoice == 2) {
                /* Pokaż rekord i*/
                System.out.println("Podaj numer indeksu: ");
                int i = intSc.nextInt();
                try {
                    StudentResolver sr = new StudentResolver();
                    Student stud = sr.get(i);
                    System.out.println(stud.toString());
                } catch (NullPointerException e) {
                    System.out.println("Wskazany rekord nie istnieje.");
                }
            }
            if (usrChoice == 3) {
                /* Dodaj rekord. */
                Map<String, String> studMap = new HashMap<>();
                System.out.println("Podaj imię: ");
                String newName = strSc.nextLine().trim();
                System.out.println("Podaj nazwisko: ");
                String newLastName = strSc.nextLine().trim();
                String name = "name";
                String lastName = "lastname";
                studMap.put(name, newName);
                studMap.put(lastName, newLastName);
                StudentResolver sr = new StudentResolver();
                System.out.println("Imię " + studMap.get("name") + " nazwisko " + studMap.get("lastname"));
                if (sr.insert(studMap)) {
                    System.out.println("Udało się dodać rekord!");
                } else {
                    System.out.println("Nie udało się dodać rekordu, sprawdź czy już nie istnieje.");
                }
            }

            if (usrChoice == 4) {
                System.out.println("Podaj id rekordu do usunięcia.");
                int i = intSc.nextInt();
                StudentResolver sr = new StudentResolver();
                if (sr.delete(i)) {
                    System.out.println("Udało się usunąć rekord.");
                } else {
                    System.out.println("Nie udało się usunąć wskazanego rekordu.");
                }
            }

            if (usrChoice == 5) {
                /* Nadpisz rekord. */
                StudentResolver sr = new StudentResolver();
                System.out.println("Podaj id rekordu do nadpisania: ");
                int i = intSc.nextInt();
                Map<String, String> studMap = new HashMap<>();
                System.out.println("Podaj imię: ");
                String newName = strSc.nextLine().trim();
                System.out.println("Podaj nazwisko: ");
                String newLastName = strSc.nextLine().trim();
                String name = "name";
                String lastName = "lastname";
                studMap.put(name, newName);
                studMap.put(lastName, newLastName);
                if (sr.update(i, studMap)) {
                    System.out.println("Udało się zaktualizować rekord!");
                } else {
                    System.out.println("Nie udało się zaktualizować rekordu o podanym numerze id.");
                }

            }

        }
        strSc.close();
        intSc.close();
    }

    private static void UserMenu() {
        System.out.println("Menu użytkownika: ");
        System.out.println("1. Pokaż rekordy.");
        System.out.println("2. Pokaż rekord [id= ]");
        System.out.println("3. Dodaj rekord.");
        System.out.println("4. Usuń rekord [id= ]");
        System.out.println("5. Nadpisz rekord [id= ]");
        System.out.println("6. Koniec.");
    }
}
