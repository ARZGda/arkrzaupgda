package connector;

/**
 * Created by RENT on 2017-07-06.
 */
public class DbConnector {
    private String dbhost = "localhost";
    private String dbname;
    private String dbuser = "root";
    private String dbpass = "";
    private int dbport = 3306;

    public DbConnector() {
    }

    public String hostname() {
        return dbhost;
    }

    public DbConnector hostname(String dbhost) {
        this.dbhost = dbhost;
        return this;
    }

    public String database() {
        return dbname;
    }

    public DbConnector database(String dbname) {
        this.dbname = dbname;
        return this;
    }

    public String username() {
        return dbuser;
    }

    public DbConnector username(String dbuser) {
        this.dbuser = dbuser;
        return this;
    }

    public String password() {
        return dbpass;
    }

    public DbConnector password(String dbpass) {
        this.dbpass = dbpass;
        return this;
    }

    public int port() {
        return dbport;
    }

    public DbConnector port(int dbport) {
        this.dbport = dbport;
        return this;
    }
}
