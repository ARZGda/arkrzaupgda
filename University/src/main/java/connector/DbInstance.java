package connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by RENT on 2017-07-06.
 */
public class DbInstance {
    private static DbInstance _instance = null;
    private Connection _connection;

    private DbInstance(DbConnector dcb) throws SQLException {
        String url = "jdbc:mysql://" + dcb.hostname() + ":" + dcb.port() + "/" + dcb.database();

        _connection = DriverManager.getConnection(url,dcb.username(),dcb.password());
    }

    public static DbInstance getInstance() throws SQLException {
        if(_instance == null) {
            _instance = new DbInstance(new DbConnector().hostname("localhost").port(3306).username("root").password("").database("university"));
        }
        return _instance;
    }

    public Connection getConnection() {
        return _connection;
    }

}
