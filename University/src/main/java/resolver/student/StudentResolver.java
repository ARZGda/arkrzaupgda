package resolver.student;


import connector.DbInstance;
import model.Student;
import resolver.AbstractResolver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public class StudentResolver extends AbstractResolver<Student> {


    @Override
    public Student get(int i) {
        Student getStud = null;
        try {
            Connection c = DbInstance.getInstance().getConnection();
            Statement s = c.createStatement();
            String query = "SELECT * FROM `student` WHERE `id` = " + i;
            ResultSet rs = s.executeQuery(query);
            while(rs.next()){
            getStud = new Student(rs.getInt("id"),
                    rs.getString("name").trim(),
                    rs.getString("lastname").trim());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return getStud;
    }

    @Override
    public List<Student> get() {
        try {
            Connection c = DbInstance.getInstance().getConnection();
            Statement s = c.createStatement();
            List<Student> studentList = new ArrayList<>();
            String query = "SELECT * FROM `student`";

            ResultSet rs = s.executeQuery(query);
            while (rs.next()) {
                studentList.add(new Student(rs.getInt("id"),
                        rs.getString("name").trim(),
                        rs.getString("lastname").trim()));
            }
            return studentList;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public boolean delete(int i) {
        boolean ret = false;
        try {
            Connection c = DbInstance.getInstance().getConnection();
            Statement s = c.createStatement();
            ret = s.execute("DELETE FROM `student` WHERE `id` = " + i);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return ret;
    }

    @Override
    public boolean update(int id, Map params) {
        boolean ret = false;
        try {
            Connection c = DbInstance.getInstance().getConnection();
            Statement s = c.createStatement();

            ret = s.execute("UPDATE `student` SET `name` = \"" + params.get("name") + "\", `lastname` = \"" + params.get("lastname") + "\" WHERE `id` = " + id);

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(Boolean.toString(ret));
        return ret;
    }

    @Override
    public boolean insert(Map params) {
        boolean ret = false;
        try {
            Connection c = DbInstance.getInstance().getConnection();
            Statement s = c.createStatement();
            String query = "INSERT INTO student(`name`,`lastname`) VALUES (\"" + params.get("name") + "\",\"" + params.get("lastname") + "\" )";
            int res = s.executeUpdate(query);
            if (res > 0) {
                ret = true;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(Boolean.toString(ret));
        return ret;
    }
}
