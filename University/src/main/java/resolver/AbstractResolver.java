package resolver;

import java.util.List;
import java.util.Map;

/**
 * Created by RENT on 2017-07-06.
 */
public abstract class AbstractResolver<T> {

    public abstract T get(int i);

    public abstract List<T> get();

    public abstract boolean delete(int i);

    public abstract boolean insert(Map<String, String> params);

    public abstract boolean update(int id, Map<String, String> params);

}
