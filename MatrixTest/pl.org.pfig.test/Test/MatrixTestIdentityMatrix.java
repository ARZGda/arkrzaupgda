package Test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.matrix.Matrix;

public class MatrixTestIdentityMatrix {

	Matrix m;
	
	@Before
	public void init() {
		m=new Matrix();
	}
	
	@Test
	public void whenEmptyArrayIsGivenIdentityMatrixExpected() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		assertArrayEquals(expected, m.identityMatrix(actual));
	}
	
	@Test
	public void whenEmptyArrayIsGivenIdentityMatrixExpectedByValue() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		actual = m.identityMatrix(actual);
		
		for (int i=0;i<actual.length;i++) {
			for(int j=0;j<actual[i].length;j++){
				assertEquals(expected[i][j],actual[i][j]);
			}
		}
	}
	
	@Test
	public void whenEmptyArrayIsGivenCheckSizeOfArrays() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		actual = m.identityMatrix(actual);
		
		assertTrue(actual.length==expected.length);
		
		for(int i = 0;i<actual.length;i++) {
			assertTrue(actual[i].length==expected[i].length);
		}
	}

}