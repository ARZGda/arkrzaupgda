package Test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.matrix.Matrix;

public class MatrixTestIndexedMatrix {

	Matrix m;
	
	@Before
	public void init() {
		m=new Matrix();
	}
	
	@Test
	public void whenEmptyArrayIsGivenIdentityMatrixExpected() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,2,3},{4,5,6},{7,8,9}};
		assertArrayEquals(expected, m.indexedMatrix(actual));
	}
	@Test
	public void whenEmptyArrayIsGivenIdentityMatrixExpectedByValue() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,2,3},{4,5,6},{7,8,9}};
		actual = m.indexedMatrix(actual);
		
		for (int i=0;i<actual.length;i++) {
			for(int j=0;j<actual[i].length;j++){
				assertEquals(expected[i][j],actual[i][j]);
			}
		}
	}
	
	@Test
	public void whenEmptyArrayIsGivenCheckSizeOfArrays() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,0,0},{0,1,0},{0,0,1}};
		actual = m.indexedMatrix(actual);
		
		assertTrue(actual.length==expected.length);
		
		for(int i = 0;i<actual.length;i++) {
			assertTrue(actual[i].length==expected[i].length);
		}
	}
}