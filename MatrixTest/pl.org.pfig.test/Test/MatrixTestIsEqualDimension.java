package Test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.matrix.Matrix;

public class MatrixTestIsEqualDimension {

	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenTwoArraysAreGivenEqualDimensionExpected() {
		int[][] actual = new int[3][3]; 
		int[][] expected = {{1,2,3},{4,5,6},{7,8,9}};
		
		assertTrue(m.isEqualDimension(actual, expected));
	}
}
