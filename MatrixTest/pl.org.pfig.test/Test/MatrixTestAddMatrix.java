package Test;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import pl.org.pfig.matrix.Matrix;

public class MatrixTestAddMatrix {

	Matrix m;

	@Before
	public void init() {
		m = new Matrix();
	}

	@Test
	public void whenTwoArraysAreGivenSumOffArraysIsExpected() {
		int[][] actual1 = {{1,3,4},{2,5,6},{9,8,7}}; 
		int[][] actual2 = {{1,2,3},{4,5,6},{7,8,9}};
		int[][] expected = {{2,5,7},{6,10,12},{16,16,16}};
		assertArrayEquals(expected,m.addMatrix(actual1, actual2));
	}
	
}
