import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class LinkedQueueExample implements QueueInterface {

    private Element first;
    private Element last;
    private int number;

    public boolean isEmpty() {
        return first == null;

    }

    public void offer(int value) {
        Element element = new Element();
        element.value = value;
        if (number == 0) {
            first = element;
            last = first;
        } else {
            last.next = element;
        }
        last = element;
        number++;
    }


    public Integer poll() {
        int result = peek();

        first = first.next;
        if (first == null) {
            last = null;
        }
        return result;
    }

    public Integer peek() {
        if (!isEmpty()) {
            return first.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    private static class Element {
        Element next;
        int value;
    }
}
