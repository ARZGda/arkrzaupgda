import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleQueueExamplePlus<T> implements QueueInterface<T> {

    private Object[] data;
    private int start = 0;
    private int end = 0;
    boolean isEmpty = true;

    public SimpleQueueExamplePlus() {
        this(8);
    }

    public SimpleQueueExamplePlus(int size) {
        data = new Object[size];
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void offer(T value) {
        if (start != end || isEmpty) {
            data[end] = value;

        } else {
            int i = 0;
            Object[] newData = new Object[data.length * 2];
            for (i = i; i < data.length; i++) {
                newData[i] = data[i];
            }
            data = newData;
            start = 0;
            end = i;

        }
        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    public T poll() {

        if (!isEmpty) {
            start = (start + 1) % data.length;
            if (start == end) {
                isEmpty = true;
            }
        }
        if (start == 0) {
            return (T) data[data.length];
        } else if (start < data.length) {
            return (T) data[start - 1];
        } else {
            throw new NoSuchElementException();
        }
    }

    public T peek() {
        if (!isEmpty) {
            return (T) data[start];
        } else {
            throw new NoSuchElementException();
        }

    }
}
