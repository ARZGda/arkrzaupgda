import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class LinkedQueueExamplePlus<T> implements QueueInterface<T> {

    private Element first;
    private Element last;
    private int number;

    public boolean isEmpty() {
        return first == null;

    }

    public void offer(T value) {
        Element element = new Element();
        element.value = value;
        if (number == 0) {
            first = element;
            last = first;
        } else {
            last.next = element;
        }
        last = element;
        number++;
    }


    public T poll() {
        T result = peek();

        first = first.next;
        if (first == null) {
            last = null;
        }
        return result;
    }

    public T peek() {
        if (!isEmpty()) {
            return (T) first.value;
        } else {
            throw new NoSuchElementException();
        }
    }

    private static class Element<T> {
        Element next;
        T value;
    }
}
