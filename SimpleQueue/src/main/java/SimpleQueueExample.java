import java.util.NoSuchElementException;

/**
 * Created by RENT on 2017-06-27.
 */
public class SimpleQueueExample implements QueueInterface {

    private int[] data;
    private int start = 0;
    private int end = 0;
    boolean isEmpty = true;

    public SimpleQueueExample() {
        this(8);
    }

    public SimpleQueueExample(int size) {
        data = new int[size];
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void offer(int value) {
        if (start != end || isEmpty) {
            data[end] = value;

        } else {
            int i = 0;
            int[] newData = new int[data.length * 2];
            for (i = i; i < data.length; i++) {
                newData[i] = data[i];
            }
            data = newData;
            start = 0;
            end = i;

        }
        data[end] = value;
        end = (end + 1) % data.length;
        isEmpty = false;
    }

    public Integer poll() {

        if (!isEmpty) {
            start = (start + 1) % data.length;
            if (start == end) {
                isEmpty = true;
            }
        }
        if (start == 0) {
            return data[data.length];
        } else if (start < data.length) {
            return data[start - 1];
        } else {
            throw new NoSuchElementException();
        }
    }

    public Integer peek() {
        if (!isEmpty) {
            return data[start];
        } else {
            throw new NoSuchElementException();
        }

    }
}
