SET SQL_SAFE_UPDATES=0;
DROP DATABASE school;
CREATE DATABASE school;

CREATE TABLE teacher (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
surname VARCHAR(30)
);

CREATE TABLE 'subject' (
id INT AUTO_INCREMENT PRIMARY KEY,
subjects_name VARCHAR(30)
);

CREATE TABLE lesson_number (
id INT AUTO_INCREMENT PRIMARY KEY,
lessons_start time,
lessons_end time
);

CREATE TABLE lesson (
PRIMARY KEY (lesson_number_id,teacher_id),
subject_id INT,
lesson_number_id INT,
teacher_id INT, 
FOREIGN KEY(subject_id) REFERENCES 'subject'(id),
FOREIGN KEY(lesson_number_id) REFERENCES lesson_number(id),
FOREIGN KEY(teacher_id) REFERENCES teacher(id)
);

