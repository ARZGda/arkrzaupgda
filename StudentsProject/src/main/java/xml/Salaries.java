package xml;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Salaries {

	static Document doc;
	private final static String FILE_NAME = "resources/xml/staff.xml";

	public static void main(String argv[]) {

		try {
			Document doc = XMLUtils.parseFile(FILE_NAME);
			printStaffInfo(doc);
			System.out.println("Average salary: " + avgSalary(doc));
			System.out.println("Personal List: ");
			System.out.println(getList(doc));
			getMinMaxStaffID(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void printStaffInfo(Document doc) {
		NodeList getPerson = doc.getElementsByTagName("staff");
		for (int i = 0; i < getPerson.getLength(); i++) {
			Node person = getPerson.item(i);
			printPersonalInfo(person);
		}

	}

	static void printPersonalInfo(Node currentPerson) {
		System.out.println("\nCurrent Element :" + currentPerson.getNodeName());
		if (currentPerson.getNodeType() == Node.ELEMENT_NODE) {
			Element person = (Element) currentPerson;
			printPersonalRecord(person);
		}
	}

	private static void printPersonalRecord(Element person) {
		System.out.println("Personal id : " + person.getAttribute("id"));
		System.out.println("First Name : " + XMLUtils.getText(person, "firstname"));
		System.out.println("Last Name : " + XMLUtils.getText(person, "lastname"));
		System.out.println("Salary : " + XMLUtils.getText(person, "salary"));
	}

	private static double avgSalary(Document doc) {
		NodeList getPerson = doc.getElementsByTagName("staff");
		double salarySum = 0;
		int div = 0;
		for (int i = 0; i < getPerson.getLength(); i++) {
			Node personN = getPerson.item(i);
			if (personN.getNodeType() == Node.ELEMENT_NODE) {
				Element person = (Element) personN;
				salarySum += Integer.parseInt(XMLUtils.getText(person, "salary"));
				div++;
			}
		}
		return (Double) (salarySum / div);
	}

	public static ArrayList<String> getList(Document doc) {
		NodeList getPerson = doc.getElementsByTagName("staff");
		ArrayList<String> personList = new ArrayList<>();
		for (int i = 0; i < getPerson.getLength(); i++) {
			Node personN = getPerson.item(i);
			if (personN.getNodeType() == Node.ELEMENT_NODE) {
				Element person = (Element) personN;
				personList.add(XMLUtils.getText(person, "firstname") + " " + XMLUtils.getText(person, "lastname"));
			}
		}
		return personList;
	}

	public static void getMinMaxStaffID(Document doc) {
		NodeList getPerson = doc.getElementsByTagName("staff");
		int[] maxMinArray = new int[getPerson.getLength()];

		for (int i = 0; i < getPerson.getLength(); i++) {
			Node personN = getPerson.item(i);
			if (personN.getNodeType() == Node.ELEMENT_NODE) {
				Element person = (Element) personN;
				maxMinArray[i] = Integer.parseInt(person.getAttribute("id"));
			}
		}
		int max = 0, min = maxMinArray[0];
		for (int i = 0; i < maxMinArray.length; i++) {
			if (maxMinArray[i] > max) {
				max = maxMinArray[i];
			}
			if (maxMinArray[i] < min) {
				min = maxMinArray[i];
			}
		}
		System.out.println("Id max: " + max);
		System.out.println("Id min: " + min);
	}
}
