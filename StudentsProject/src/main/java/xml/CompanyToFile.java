package xml;

import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CompanyToFile {
	static Document doc;
	private final static String FILE_NAME = "resources/xml/companies.xml";

	public static void main(String argv[]) {

		create();
	}

	public static void create() throws TransformerFactoryConfigurationError {
		try {
			doc = XMLUtils.newFile();
			createContent();
			XMLUtils.saveFile(doc, FILE_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void createContent() {
		int num = 0;
		Element rootElement = createRootElement();
		Element students = createStudentsElement(rootElement);
		addStudent(num,students, "John", "Simple", 3);
		addStudent(num,students, "Jane", "Doe", 1);
	}

	public static void addStudent(int num,Element students, String name, String surname, int year) {
		Element student = createStudentElement(students);
		Element studentName = doc.createElement("name");
		Element studentSurname = doc.createElement("surname");
		Element studentYear = doc.createElement("year");
		createStudents(name, surname, year, studentName, studentSurname, studentYear);
		appendStudents(student, studentName, studentSurname, studentYear);
	}

	public static void createStudents(String name, String surname, int year, Element studentName,
			Element studentSurname, Element studentYear) {
		studentName.appendChild(doc.createTextNode(name));
		studentSurname.appendChild(doc.createTextNode(surname));
		studentYear.appendChild(doc.createTextNode("" + year));
	}

	public static void appendStudents(Element student, Element studentName, Element studentSurname,
			Element studentYear) {
		student.appendChild(studentName);
		student.appendChild(studentSurname);
		student.appendChild(studentYear);
	}

	public static Element createStudentElement(Element students){
		Element student = doc.createElement("student");
		students.appendChild(student);
		return student;
	}
	public static Element createStudentsElement(Element rootElement) {
		Element students = doc.createElement("students");
		rootElement.appendChild(students);
		return students;
	}

	public static Element createRootElement() {
		Element rootElement = doc.createElement("Root");
		doc.appendChild(rootElement);
		return rootElement;
	}
}
