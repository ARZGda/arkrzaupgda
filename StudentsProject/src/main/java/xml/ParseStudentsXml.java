package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ParseStudentsXml {
	
	private static final String FILE_NAME = "Resources/xml/class.xml";
	public static void main(String[] args) {
		
		
		
		try {
			Document doc = XMLUtils.parseFile(FILE_NAME);
			ParseStudentsXml.printStudentsInfo(doc);
	} catch (Exception e) {
				e.printStackTrace();
		}
	}

	static void printStudentInfo(Node currentStudent) {
		System.out.println("\nCurrent Element :" + currentStudent.getNodeName());
		if (currentStudent.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) currentStudent;
			printStudentRecord(student);
		}
	}

	private static void printStudentRecord(Element student) {
		System.out.println("Student roll no : " + student.getAttribute("rollno"));
		System.out.println("First Name : " + XMLUtils.getText(student,"firstname"));
		System.out.println("Last Name : " + XMLUtils.getText(student,"lastname"));
		System.out.println("Nick Name : " + XMLUtils.getText(student,"nickname"));
		System.out.println("Marks : " + XMLUtils.getText(student,"marks"));
	}

	public static void printStudentsInfo(Document doc) {
		NodeList getStudent = doc.getElementsByTagName("student");
		System.out.println("----------------------------");
		for (int i = 0; i < getStudent.getLength(); i++) {
			Node student = getStudent.item(i);
			printStudentInfo(student);
		}
	}


}
