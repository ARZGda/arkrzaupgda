package xml;

import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreatorCarToFile {
	static Document doc;
	private final static String FILE_NAME =  "resources/xml/cars.xml";
	public static void main(String argv[]) {

		create();
	}

	public static void create() throws TransformerFactoryConfigurationError {
		try {
			doc = XMLUtils.newFile();
			createContent();
			XMLUtils.saveFile(doc,FILE_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void createContent() {
		Element rootElement = createRootElement();
		Element supercars = createSupercarElement(rootElement);
		XMLUtils.setAttribute(doc, supercars, "company", "Ferrari");
		addCar(supercars, "formula one", "Ferrari 101");
		addCar(supercars, "sports", "Ferrari 102");
	}

	public static void addCar(Element supercar, String type, String name) {
		Element carname = doc.createElement("carname");
		XMLUtils.setAttribute(doc, supercar, "type", type);
		carname.appendChild(doc.createTextNode(name));
		supercar.appendChild(carname);
	}

	public static Element createSupercarElement(Element rootElement) {
		Element supercars = doc.createElement("supercars");
		rootElement.appendChild(supercars);
		return supercars;
	}

	public static Element createRootElement() {
		Element rootElement = doc.createElement("cars");
		doc.appendChild(rootElement);
		return rootElement;
	}
}