package tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;

import javax.management.RuntimeErrorException;

public class MyScanner {

	private BufferedReader myRead;
	
	public MyScanner() {
		this(System.in);
	}
	public MyScanner(InputStream in) {
		
	}
	public int nextInt() {
		try {
			return Integer.parseInt(myRead.readLine());
		}catch (NumberFormatException | IOException e) {
			throw new RuntimeErrorException(e);	
		}
	}
}
