package students;

import java.util.List;

public interface FileInterface {

	public void save(List<Student> studentsList);

	public List<Student> load();

}