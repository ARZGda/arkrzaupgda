package students;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class University {

	private Map<Integer, Student> students = new HashMap<>();

	public University(List<Student> students) {
			for (Student student : students) {

				this.students.put(student.getIndexNumber(), student);
			}
		
	}

	public List<Student> getStudentsList() {

		List<Student> sList = new LinkedList<>();

		for (Student s : students.values()) {

			sList.add(s);
		}

		return sList;
	}

	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}

	public List<Student> toList() {
		List<Student> studentsList = new ArrayList<>();
		for (Student s : students.values()) {
			studentsList.add(s);
		}
		return studentsList;
	}

	public boolean studentExists(int indexNumber) {
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		return students.get(indexNumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + ", " + s.getSurname());
		}
	}
}