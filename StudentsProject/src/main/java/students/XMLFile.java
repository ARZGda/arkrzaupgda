package students;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import xml.XMLUtils;

public class XMLFile implements FileInterface {
	private final static String FILE_NAME = "printedtoxml.xml";

	@Override
	public void save(List<Student> studentsList) {
		try {
			Document doc = XMLUtils.newFile();
			createContent(doc, studentsList);
			XMLUtils.saveFile(doc, FILE_NAME);
		} catch (TransformerFactoryConfigurationError | TransformerException | ParserConfigurationException e) {
			System.out.println(e.getMessage());
		}
	}

	private void createContent(Document doc, List<Student> studentsList) {
		Element students = doc.createElement("students");
		doc.appendChild(students);
		for (Student student : studentsList) {
			addStudent(doc, students, student);
		}

	}

	public void addStudent(Document doc, Element students, Student student) {
		Element studentElement = doc.createElement("student");
		XMLUtils.setAttribute(doc, studentElement, "index", String.valueOf(student.getIndexNumber()));
		students.appendChild(studentElement);

		Element name = doc.createElement("name");
		Element surname = doc.createElement("surname");

		name.appendChild(doc.createTextNode(student.getName()));
		surname.appendChild(doc.createTextNode(student.getSurname()));

		studentElement.appendChild(name);
		studentElement.appendChild(surname);
	}

	@Override
	public List<Student> load() {
		try {
			Document doc = XMLUtils.parseFile(FILE_NAME);
			printStudentsInfo(doc);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	static void printStudentsInfo(Document doc) {
		NodeList getStudent = doc.getElementsByTagName("student");
		System.out.println("----------------------------");
		for (int i = 0; i < getStudent.getLength(); i++) {
			Node student = getStudent.item(i);
			printStudentInfo(student);
		}
	}

	static void printStudentInfo(Node currentStudent) {
		System.out.println("\nCurrent Element :" + currentStudent.getNodeName());
		if (currentStudent.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) currentStudent;
			printStudentRecord(student);
		}
	}

	static void printStudentRecord(Element student) {
		System.out.println("Student roll no : " + student.getAttribute("index"));
		System.out.println("First Name : " + XMLUtils.getText(student, "name"));
		System.out.println("Last Name : " + XMLUtils.getText(student, "surname"));
	}
	
}
