package students;
import java.util.HashMap;
import java.util.Map;

public class StudentsIndexMap {
	
	public static void main(String[] args) {

		Map<Integer, Student> studentByIndex = new HashMap<>();
		studentByIndex.put(10900, new Student(10900, "Jan", "Kowalski"));
		studentByIndex.put(10400, new Student(10400, "Bartek", "Pieprzy�ski"));
		studentByIndex.put(10200, new Student(10200,"Joachim","Artegor"));
		studentByIndex.put(10100, new Student(10300, "Herbert" , "Angdon"));
		
		for (Student student : studentByIndex.values()) {
			System.out.println(student.getIndexNumber() + " " + student.getName() + " " + student.getSurname());
		}
		
//		System.out.println(studentByIndex.containsKey(10300));
//		System.out.println(studentByIndex.size());
//		System.out.println(studentByIndex.get(10400).getName());
	}
	
}