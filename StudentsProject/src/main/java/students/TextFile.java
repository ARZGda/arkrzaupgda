package students;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements FileInterface {

	private final static String FILE_NAME = "students_text.txt";
	
	/* (non-Javadoc)
	 * @see students.FileInterface#save(java.util.List)
	 */
	@Override
	public void save(List<Student> studentsList) {
		try {
			PrintStream printStream = new PrintStream(FILE_NAME);
			for (Student student : studentsList) {
				printStream.println(student.getIndexNumber()+" "+student.getName()+" "+student.getSurname());
			}
			printStream.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		
	}

	/* (non-Javadoc)
	 * @see students.FileInterface#load()
	 */
	@Override
	public List<Student> load() {
		List<Student> students = new ArrayList<>();
		try {
			Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream(FILE_NAME)));
			
			while (!scanner.hasNextLine())
			{
				String currentLine;
				currentLine = scanner.nextLine();
				String[] record = currentLine.split(" ");
				students.add(new Student(Integer.parseInt(record[0]),record[1],record[2]));
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return students;
		
	}

}