package students;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JasonFile implements FileInterface {

	private final static String FILE_NAME = "jason.txt";
	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public void save(List<Student> studentsList) {
		try {
			mapper.writeValue(new File (FILE_NAME), studentsList);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public List<Student> load() {
		
		try {
			List<Student> students = mapper.readValue(new File(FILE_NAME),new TypeReference<List<Student>>() {} );
			return students;
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
		return null;
	}

}
