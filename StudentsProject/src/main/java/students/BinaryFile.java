package students;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BinaryFile implements FileInterface {
	private final static String FILE_NAME = "binary.txt";

	@Override
	public void save(List<Student> studentsList) {
			FileOutputStream foStream;
			try {
			foStream = new FileOutputStream(FILE_NAME);
			BufferedOutputStream boStream = new BufferedOutputStream(foStream);
			DataOutputStream out = new DataOutputStream(boStream);
			for (Student student : studentsList) {
				out.writeInt(student.getIndexNumber());
				out.writeUTF(student.getName());
				out.writeUTF(student.getSurname());
			}
			out.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}

	@Override
	public List<Student> load() {
		List<Student> students = new ArrayList<>();
		try {
			DataInputStream scanner = new DataInputStream(new BufferedInputStream(new FileInputStream(FILE_NAME)));
			
			while (scanner.available() > 0)
			{
				int index = scanner.readInt();
				String name = scanner.readUTF();
				String surname = scanner.readUTF();
				students.add(new Student(index, name, surname));
			}
			
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return students;

	}

}
