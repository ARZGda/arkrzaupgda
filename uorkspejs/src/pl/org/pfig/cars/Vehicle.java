package pl.org.pfig.cars;

public class Vehicle extends Car {

	private String name;
	private String engine;
	private int tires;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getTires() {
		return tires;
	}
}
