package pl.org.pfig.cars;

public abstract class Car {

	public abstract String getName();
	public abstract int getTires();
	
}
