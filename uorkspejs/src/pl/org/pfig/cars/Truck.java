package pl.org.pfig.cars;

public class Truck extends Car {

	private String name;
	private String engine;
	private int tires;
	
	public Truck(String name, int tires, String engine){
		this.name = name;
		this.tires = tires;
		this.engine = engine;
		
	}
	@Override
	public String getName() {
		return this.name;
	}
	@Override
	public int getTires() {
		return this.tires;
	}
	
}
