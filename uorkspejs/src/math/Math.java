package math;

public class Math {

	public static void main(String[] args) {
		int[] tabunia = { 1, 3, 2, 1, 3, 5, 2, 3 };

		Suma(tabunia);

		Iloczyn(tabunia);

		Silnia(tabunia);

		int arytm = 0;
		int roznc = 5;
		int ostwyr = 3;
		int sumaar = 0;
		int a1 = 0;

		int a = a1;
		for (int i = 1; i <= ostwyr; i++) {
			arytm = arytm + a;
			a = a + roznc;
		}
		System.out.println("Suma arytmetyczna = " + arytm);

		arytm = 0;
		a1 = 0;
		sumaar = (2 * a1 + (ostwyr - 1) * roznc) / 2 * ostwyr;
		System.out.println("Suma arytmetyczna ci�gu o pierwszym wyrazie " + a1 + ", r�nicy " + roznc + " i " + ostwyr
				+ " wyra�eniach wynosi " + sumaar);

	}

	private static void Suma(int[] tabunia) {
		int suma = 0;

		for (int i : tabunia) {
			suma += i;
		}
		System.out.println("Suma : " + suma);
	}

	private static void Silnia(int[] tabunia) {
		int silnia = 1;
		for (int i : tabunia) {
			for (int j = 1; j <= i; j++) {
				silnia *= j;
			}
			System.out.println("Silnia " + i + " jest r�wna : " + silnia);
			silnia = 1;
		}
	}

	private static void Iloczyn(int[] tabunia) {
		int iloczyn = 1;
		for (int i : tabunia) {
			iloczyn *= i;
		}
		System.out.println("Iloczyn : " + iloczyn);
	}

}
