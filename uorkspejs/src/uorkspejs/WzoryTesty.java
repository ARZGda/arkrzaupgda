package uorkspejs;

import org.junit.Test;

public class WzoryTesty {

	@Test
	public void testsqA()
	{
		assert WzoryMath.squareArea(6)==36;
		assert WzoryMath.squareArea(2)==4;
	}
	
	@Test
	public void testcuA()
	{
		assert WzoryMath.cubeArea(6.0)==216.0;
		assert WzoryMath.cubeArea(2)==24;
		assert WzoryMath.cubeArea(0.5)==1.5;
	}
	@Test
	public void testciA(){
		assert WzoryMath.circleArea(1)>3.1415;
		assert WzoryMath.circleArea(1)<3.1416;
	}
	@Test
	public void testroC(){
		assert WzoryMath.rollCapacity(1, 1)>3.1415;
		assert WzoryMath.rollCapacity(1, 1)<3.1416;
	}
	@Test
	public void testcoC(){
		assert WzoryMath.coneCapacity(1, 1)*3>3.1415;
		assert WzoryMath.coneCapacity(1, 1)*3<3.1416;
	}
	@Test
	public void testcuC(){
		assert WzoryMath.cubeCapacity(3)==27;
		assert WzoryMath.cubeCapacity(1)==1;
	}
	
	@Test
	public void testpyC(){
		assert WzoryMath.pyramidCapacity(1, 1)*3==1;
		assert WzoryMath.pyramidCapacity(3, 1)==1;
	}
	
	
}
