package uorkspejs;

import org.junit.Test;

public class metodydodyTest {

	
	
	@Test
	public void testIloczDwoch(){
		assert metodydody.iloczDwoch(1, 1)==1;
		assert metodydody.iloczDwoch(0, 1)==0;
		assert metodydody.iloczDwoch(1, 10)==10;
		assert metodydody.iloczDwoch(3, 4)==12;
	}
	@Test
	public void testSumaDwoch(){
		assert metodydody.sumaDwoch(3, 4)==7;
		assert metodydody.sumaDwoch(-3, 8)==5;
		assert metodydody.sumaDwoch(-9,0)==-9;
	}
	@Test
	public void testMniejszaZDw(){
		assert metodydody.sprawdzMniejsze(9, -2)==-2;
		assert metodydody.sprawdzMniejsze(0,7)==0;
		assert metodydody.sprawdzMniejsze(-4, -8)==-8;
	}
	
}
