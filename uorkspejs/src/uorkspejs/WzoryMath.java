package uorkspejs;

public class WzoryMath {

	public static double squareArea(double a) {
		return a * a;
	}

	public static double cubeArea(double a) {
		return squareArea(a) * 6;
	}

	public static double circleArea(double r) {
		return Math.PI * r * r;
	}

	public static double rollCapacity(double h, double r) {
		return circleArea(r) * h;
	}

	public static double coneCapacity(double h, double r) {
		return rollCapacity(h, r) / 3;
	}

	public static double cubeCapacity(double a) {
		return squareArea(a) * a;
	}

	public static double pyramidCapacity(double h, double a) {
		return squareArea(a) * h / 3;
	}

}
