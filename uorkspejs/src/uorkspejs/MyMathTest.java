package uorkspejs;

import org.junit.Test;

public class MyMathTest {

	@Test
	public void absIntegerTest(){
		assert MyMath.abs(4)==4;
		assert MyMath.abs(-4)==4;
		assert MyMath.abs(0)==0;
	}
	@Test
	public void absDoubleTest(){
		assert MyMath.abs(4.0)==4.0;
		assert MyMath.abs(-4.5)==4.5;
		assert MyMath.abs(0.0)==0.0;
	}
	@Test
	public void abslongTest(){
		assert MyMath.abs(4l)==4;
		assert MyMath.abs(-4l)==4;
		assert MyMath.abs(0l)==0;
	}
	@Test
	public void absfloatTest(){
		assert MyMath.abs(4.0f)==4.0;
		assert MyMath.abs(-4.5f)==4.5;
		assert MyMath.abs(0.0f)==0.0;
	}
@Test
	public void powIntegerTest(){
	assert MyMath.pow(2,2)==4;
	assert MyMath.pow(3,4)==3*3*3*3;
}
@Test
	public void powdoubleTest(){
assert MyMath.pow(2.4,2)==2.4*2.4;
assert MyMath.pow(3.1,4)==3.1*3.1*3.1*3.1;
}
@Test
	public void powFloatTest(){
		assertInRange(MyMath.pow(2.4f,2), 5.75999, 5.76001);
}

	private void assertInRange(double result, double down, double up ) {
		assert result > down;
		assert result < up;
}
}

