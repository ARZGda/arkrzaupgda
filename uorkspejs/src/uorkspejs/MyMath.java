package uorkspejs;

public class MyMath {

	public static void main(String[] args) {

		System.out.println(pow(4,1));
	}

	public static int max(int a, int b)
	{
		if (a<=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	public static long max(long a, long b)
	{
		if (a<=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	public static float max(float a, float b)
	{
		if (a<=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	public static double max(double a, double b)
	{
		if (a<=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	
	public static int min(int a, int b)
	{
		if (a>=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	public static long min(long a, long b)
	{
		if (a>=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	public static float min(float a, float b)
	{
		if (a>=b)
		{
			return b;
		} else
		{
			return a;
		}
	}
	public static double min(double a, double b)
	{
		if (a>=b)
		{
			return b;
		} else
		{
			return a;
		}
	}

	public static int abs(int a)
	{
		if (a<0)
		{
			a=-a;
		}
		return a;
	}
	public static long abs(long a)
	{
		if (a<0)
		{
			a=-a;
		}
		return a;
	}
	public static float abs(float a)
	{
		if (a<0)
		{
			a=-a;
		}
		return a;
	}
	public static double abs(double a)
	{
		if (a<0)
		{
			a=-a;
		}
		return a;
	}

	public static long pow(int a, int b)
	{
		long pow=1;
		for (int i =1;i<=b;i++)
		{
			pow*=a;
		}
		return pow;
	}
	public static long pow(long a, int b)
	{
		long pow=1;
		for (int i =1;i<=b;i++)
		{
			pow*=a;
		}
		return pow;
	}
	public static double pow(float a, int b)

	{
		double pow=1.0;
		for (int i =1;i<=b;i++)
		{
			pow*=a;
		}
		return pow;
	}
	public static double pow(double a, int b)
	{
		double pow=1.0;
		for (int i =1;i<=b;i++)
		{
			pow*=a;
		}
		return pow;
	}
}
