package uorkspejs;

public class zet2 {

	public static void main(String[] args) {

		System.out.print("2+3 = "); //a
		int a = 2+3;
		System.out.println(a);
		
		System.out.print("2-4 = "); //b
		int b = 2-4;
		System.out.println(b);
		
		System.out.print("5/2 = "); //c
		double c = 5/2;
		System.out.println(c);
		
		System.out.print("5.0/2 = "); //d
		double d = 5.0/2;
		System.out.println(d);
		
		System.out.print("5/2.0 = ");  //e
		double e = 5/2.0;
		System.out.println(e);
		
		System.out.print("5.0/2.0 = ");  //f
		double f = 5.0/2.0;
		System.out.println(f);
		
		System.out.print("100L - 10 = ");  //g
		long g = 100L - 10;
		System.out.println(g);
		
		System.out.print("2f-3 = ");  //h
		float h = 2f-3;
		System.out.println(h);
		
		System.out.print("5f/2 = ");  //i
		float i = 5f/2;
		System.out.println(i);
		
		System.out.print("5d/2 = ");  //j
		double j = 5d/2;
		System.out.println(j);
		
		System.out.print("'A'+2 = ");  //k
		int k = 'A'+2;
		System.out.println(k);
		
		System.out.print("'a'+2 = ");  //l
		int l = 'a'+2;
		System.out.println(l);
		
		System.out.print("\"a\"+2 = ");  //m
		String m = "a" + 2;
		System.out.println(m);
		
		System.out.print("\"a\"+\"b\" = ");  //n
		String n = "a"+'b';
		System.out.println("a"+"b");
		
		System.out.print("'a'+'b' = ");  //o
		int o = 'a'+'b';
		System.out.println(o);
		
		System.out.print("\"a\"+ 'b' = ");  //p
		String p = "a"+'b';
		System.out.println("a"+'b');
		
		System.out.print("\"a\"+'b'+3 = ");  //q
		String q = "a" + 'b' +3;
		System.out.println(q);
		
		System.out.print("'b'+3+\"a\" = ");  //r
		String r = (int)'b'+3 + "a";
		System.out.println(r);
		
		System.out.print("9%4 = ");  //s
		int s = 9%4;
		System.out.println(s);		
		

	}

}
