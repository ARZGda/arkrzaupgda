package pl.org.pfig;

import java.util.Random;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		// Main.printAddresses("Algorytmiczna",4);
		System.out.println(Main.guessNumber(10, 100));
	}

	public static void printAddresses(String street, int num) {
		char klatka = 'a';
		for (int i = 1; i <= num; i += 2) {
			for (int j = 1; j <= 12; j++) {
				if (j > 6) {
					klatka = 'b';
				}
				System.out.println(street + " " + i + klatka + " m." + j);
			}
		}
	}

	public static String guessNumber(int a, int b) {
		Scanner sc = new Scanner(System.in);
		Random number = new Random();
		int answer = number.nextInt(b + 1);
		int guess = 0;
		System.out.println("Spróbuj zgadnąć, liczba jest między " + a + " i " + b);
		for (int i = 1; i <= 10; i++) {
			guess = sc.nextInt();

			if ((guess != answer) && (i < 10))
			
			{
				System.out.println("Spróbuj jeszcze raz! To Twoja " + (i + 1) + " próba!");
			} 
			
			else if ((guess != answer) && (i == 10)) 
			
			{		
				System.out.println("To była ostatnia próba!");
			} 
			else 
			{
				sc.close();
				return "Brawo! Zgadłeś";
			}

		}
		sc.close();
		return "Niestety, nie zgadłeś!";
	}
}