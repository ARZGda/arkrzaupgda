/**
 * Created by RENT on 2017-06-26.
 */
public class MyMath {

    public static int sum(int a, int b){
        return a+b;
    }

public static int findBisection(int[] data, int value){
        int a=0;
        int b=data.length;
        int s;
        while (a<b){
            s=(a+b)/2;
            if (value<data[s]){
                b=s;
            } else if (value==data[s]) {
                return s;
            } else {
                a=s;
            }
        }
        return -1;
}
}
