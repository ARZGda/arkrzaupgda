<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title> MyNewestwebSite </title>
    <link rel="stylesheet" href="style.css" />
</head>
<body>
<div id="container">
    <header>
        Nagłówek
        <ul>
            <li><a href="index.jsp">Strona główna</a></li>
            <li><a href="add.jsp">Dodaj film</a> </li>
            <li><a href="MovieServlet?action=show">Przeglądaj filmy</a> </li>
        </ul>
    </header>
