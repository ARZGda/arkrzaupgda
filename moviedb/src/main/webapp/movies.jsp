<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:import url="header.jsp" />

<section>
    <table>
        <tr>
            <td> id </td>
            <td> name </td>
            <td> actions </td>
    <c:forEach items="${movies}" var="movie">
        <tr>
            <td><p><c:out value="${movie.id}"/></p></td>
            <td><p><c:out value="${movie.name}"/></p></td>
            <td><a href="edit.jsp">edytuj</a> <a href="MovieServlet?action=delete"> usuń</a> </td>
        </tr>
    </c:forEach>
    </table>
</section>

<c:import url="footer.jsp" />