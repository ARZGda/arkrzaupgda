package pl.arek.moviedb.servlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import pl.arek.moviedb.entity.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by RENT on 2017-07-24.
 */
@WebServlet(name = "MovieServlet")
public class MovieServlet extends HttpServlet {

    SessionFactory sf = new Configuration().configure().buildSessionFactory();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
            String action = request.getParameter("action");
            if (action.equals("add")) {
                String name = request.getParameter("name");
                Session session = sf.openSession();
                Movie m = new Movie(name);
                Transaction t = session.beginTransaction();
                session.save(m);
                t.commit();
                session.close();
            }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
        String action = request.getParameter("action");
        Session session = sf.openSession();
        if(action.equals("show")) {
            Transaction t = session.beginTransaction();
            List<Movie> movies = session.createQuery("from Movie where id > 0").list();
            t.commit();
            request.setAttribute("movies",movies);
        }else
        if(action.equals("edit")){
            String name = request.getParameter("name");
            int id = Integer.valueOf(request.getParameter("id"));
        }else
            if(action.equals("delete")){
            int id = Integer.valueOf(request.getParameter(("id")));
            Movie m = new Movie();
            m.setId(id);
            Transaction t = session.beginTransaction();
            session.delete(m);
            }
        request.getRequestDispatcher("movies.jsp").forward(request,response);
        session.close();
    }
}
