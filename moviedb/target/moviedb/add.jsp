<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:import url="header.jsp" />

    <section>
       <h2>Dodaj film</h2>
        <form action="MovieServlet?action=add" method="POST">
            <label for="name">Podaj nazwę filmu:</label>
            <input type="text" name="name" id="name">
            <button type="submit">Dodaj</button>
        </form>
    </section>

<c:import url="footer.jsp" />