package pl.org.pfig.calc;

public class SimpleCalc {

	public int add(int a, int b) {
		return a + b;
	}
	
	public int exThrow() throws Exception {
		throw new Exception ("Wyjątek");
	}
	
	public double div(int a, int b) {
		return a/b;
	}
}
