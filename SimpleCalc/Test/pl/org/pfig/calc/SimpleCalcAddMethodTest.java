package pl.org.pfig.calc;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleCalcAddMethodTest {

	@Test
	public void whenTwoPositiveNumbersAreGivenEqualsPositiveNumber() {
		int a = 3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(9, sc.add(a, b));
	}

	@Test
	public void whenPositiveAndNegativeNumbersAreGivenEquals() {
		int a = 3, b = -6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(-3, sc.add(a, b));
	}

	@Test
	public void whenNegativeAndPositiveNumbersAreGivenEquals() {
		int a = -3, b = 6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(3, sc.add(a, b));
	}

	@Test
	public void whenTwoNegativeNumbersAreGivenEquals() {
		int a = -3, b = -6;
		SimpleCalc sc = new SimpleCalc();
		assertEquals(-9, sc.add(a, b));
	}

	@Test
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {
		SimpleCalc sc = new SimpleCalc();
		sc.exThrow();
	}

	@Test
	public void whenTwoMaxIntegersArreGivenPositiveSolutionIsExpected() {
		SimpleCalc sc = new SimpleCalc();
		assertTrue("Out of range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE) > 0);
	}
}
