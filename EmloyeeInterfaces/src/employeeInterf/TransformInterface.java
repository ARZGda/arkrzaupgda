package employeeInterf;

@FunctionalInterface

public interface TransformInterface {
	public String transform(String s);
}
