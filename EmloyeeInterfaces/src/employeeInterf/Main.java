package employeeInterf;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		
		List<Employee> emplo = new ArrayList<>();
		
		emplo.add(new Employee("Paweł","Testowy",18,3000));
		emplo.add(new Employee("Piotr","Przykladowy",32,10000));
		emplo.add(new Employee("Julia","Doe",41,4300));
		emplo.add(new Employee("Przemyslaw","Wietrak",56,4500));
		emplo.add(new Employee("Zofia","Zaspa",37,3700));

		for (Employee employee : emplo) {
			System.out.println(employee.toString());

		}

		Company c = new Company();
		c.setEmployees(emplo);
		
		c.filter(f -> f.startsWith("P"), t -> t.toUpperCase());
		
		
	}
}
