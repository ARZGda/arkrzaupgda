package pl.org.pfig.tdd;

import java.util.List;

public class Exercises {
	public int absoluteValue(int num) {
		if (num < 0) {
			num = -num;
		}
		return num;
	}

	public String determineDayOfWeek(int day) throws IllegalArgumentException {
		if (day > 7 || day < 1) {
			throw new IllegalArgumentException("Bad input value");
		}
		String[] days = { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };
		return days[day - 1];
	}

	public String determineMonth(int month) throws IllegalArgumentException {
		if (month > 12 || month < 1) {
			throw new IllegalArgumentException("Bad input value");
		}
		String[] months = { "styczen", "luty", "marzec", "kwiecien", "maj", "czerwiec", "lipiec", "sierpien",
				"wrzesien", "pazdziernik", "listopad", "grudzien" };
		return months[month - 1];
	}

	public int euclideanAlgorithm(int a, int b) {
		int c;
		while (b != 0) {
			c = a % b;
			a = b;
			b = c;
		}
		if (a < 0) {
			a = -a;
		}
		return a;
	}

	public int timeSum(int h, int m, int s) throws IllegalArgumentException {
		if ((((h < 0) || (h > 23)) || ((m < 0) || (m > 59))) || ((s < 0) || (s > 59))) {
			throw new IllegalArgumentException("Bad input value");
		}
		return 60 * (60 * h + m) + s;
	}
	
	public int[] processNumbers(int a, int b){
		if (a<0||a>255||b<0||b>255){
			throw new IllegalArgumentException();
		}
		
		int[] x = {};
		return x;
	}
}
