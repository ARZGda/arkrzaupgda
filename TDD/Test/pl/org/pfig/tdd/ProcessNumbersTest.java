package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProcessNumbersTest {

	Exercises e;
	
	@Before
	public void init() {
		e = new Exercises();
	}
	
	@Test
	public void whenArgumentAIsOutOfRangeExeptionExpected() {
		IllegalArgumentException ex = null;
		int[] arrayOfA = { -1, 256, 1000, -1000 };
		int b = 8;
		for(int a : arrayOfA) { 
			try {
				e.processNumbers(a, b);
			} catch(IllegalArgumentException e) {
				ex = e;
			}
			assertTrue(ex != null);
			ex = null;
		}
	}
	
	@Test
	public void whenArgumentBIsOutOfRangeExeptionExpected() {
		IllegalArgumentException ex = null;
		int a = 3;
		int[] arrayOfB = { -123, 257, -1, 10000 };
		for(int b : arrayOfB) {
			try {
				e.processNumbers(a, b);
			} catch(IllegalArgumentException e) {
				ex = e;
			}
			assertNotNull("Wyjątek nie wystąpił :(", ex);
			ex = null;
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void whenArgumentsAreInIncorrectOrderExeptionExpected() {
		int a = 8, b = 3;
		e.processNumbers(a, b);
	}
	
	@Test
	public void whenArgumentsAreProperExpectArrayResult() {
		int a = 3, b = 8;
		int[] expected = { 4, 6, 8, 7, 5, 3 };
		assertArrayEquals(expected, e.processNumbers(a, b));
	}
	
	@Test
	public void whenArgumentsAreTheSameExpectOneItemArray() {
		int arg = 5;
		int[] expected = { 5 };
		assertArrayEquals(expected, e.processNumbers(arg, arg));
	}

}