package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineDayOfWeekTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void ifOneToSevenGivenDayOfWeekExpected() {
		String[] expected = { "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday" };
		for (int i = 1; i < 8; i++) {
			assertEquals(expected[i - 1], e.determineDayOfWeek(i));
		}
	}

	@Test
	public void ifOutOfRangeArgumentIsGivenExceptionExpected() {
		int data = -14;
		IllegalArgumentException iae = null;
		
		try {
			e.determineDayOfWeek(data);
		} catch (IllegalArgumentException e) {
			iae = e;
		}
		
		assertNotNull("Wyjatek nie wystapil",iae);
		
	}
}
