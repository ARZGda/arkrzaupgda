package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AbsoluteValueTest {

	Exercises e;
	
	@Before
	public void init(){
		e = new Exercises();
	}
	
	@Test
	public void whenPositiveValueIsGivenPositiveValueExpected() {
		int expected = 5;
		int arg = 5;
		
		assertEquals(expected,e.absoluteValue(arg));
	}
	
	@Test
	public void whenNegativeValueIsGivenPositiveValueExpected() {
		int expected = 5;
		int arg = -5;
		
		assertEquals(expected,e.absoluteValue(arg));
	}
	
	@Test
	public void whenZeroIsGivenZeroExpected(){
		int expected = 0;
		int arg = 0;
		assertEquals(expected,e.absoluteValue(arg));
	}

}
