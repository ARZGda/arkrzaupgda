package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EuclideanAlgorithmTest {


	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}
	
	@Test
	public void ifProperValuesAreGivenProperAnswerIsExpected() {
		int[][] data = {{42,7},{-210,-6},{-210,6},{-6,210},{7,42},{0,0}};
		int[] expected = {7,6,6,6,7,0};
		
		for (int i = 0; i<data.length;i++){
			
			assertEquals(expected[i], e.euclideanAlgorithm(data[i][0], data[i][1]));
		}
	}
}