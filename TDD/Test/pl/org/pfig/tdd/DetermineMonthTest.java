package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineMonthTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void whenProperValueIsGivenProperMonthIsExpected() {
		int[] data = { 1, 12 };
		String[] expected = { "styczen", "grudzien" };

		for (int i = 0; i < data.length; i++) {
			assertEquals(expected[i], e.determineMonth(data[i]));
		}
	}

	@Test
	public void whenRandomValueIsGivenProperMonthIsExpected() {
		int data = 7;
		String expected = "lipiec";
		assertEquals(expected, e.determineMonth(data));
	}

	@Test
	public void whenOutOfRangeArgumentIsGivenExceptionExpected() {
		int data = -14;
		IllegalArgumentException iae = null;

		try {
			e.determineMonth(data);
		} catch (IllegalArgumentException e) {
			iae = e;
		}

		assertNotNull("Wyjatek nie wystapil", iae);

	}
}
