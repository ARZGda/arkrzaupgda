package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TimeSumTest {

	Exercises e;

	@Before
	public void init() {
		e = new Exercises();
	}

	@Test
	public void ifProperValuesAreGivenProperSumIsExpected() {
		int[][] data = { { 0, 0, 0 }, { 4, 23, 50 }, { 23, 59, 59 } };
		int[] expected = { 0, 15830, 86399 };
		for (int i = 0; i < data.length; i++) {

			assertEquals(expected[i], e.timeSum(data[i][0], data[i][1], data[i][2]));
		}
	}

	@Test
	public void ifNegativeOrOutOfRangeValuesAreGivenExceptionExpected() {
		int[][] data = { { -14, 10, 42 }, { 14, -10, 42 }, { 14, 10, -42 }, { 24, 10, 42 }, 
		{ 23, 60, 42 },	{ 23, 59, 60 } };
		IllegalArgumentException iae = null;
		for (int i = 0; i < data.length; i++) {
			try {

				e.timeSum(data[i][0], data[i][1], data[i][2]);
			} catch (IllegalArgumentException ex) {
				iae = ex;
			}

			assertNotNull("Wyjatek nie wystapil", iae);
			iae = null;
		}
	}

}
