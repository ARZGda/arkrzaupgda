package pl.org.basicUniversity;

/**
 * Created by RENT on 2017-06-22.
 */
public class Main {
    public static void main(String[] args) {

        ITEducation ite = new ITEducation();
        System.out.println("IT education");
        ite.general();

        PhysicalEducation pe = new PhysicalEducation();
        System.out.println("Physical Education");
        pe.general();
    }
}
