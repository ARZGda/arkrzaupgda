package pl.org.basicUniversity;

/**
 * Created by RENT on 2017-06-22.
 */
public abstract class BasicStudies {
    public void general() {
        math();
        physics();
        language();
        additional();
        System.out.println();
    }

    public void math() {
        System.out.println("Math studies");
    }

    public void physics() {
        System.out.println("Physics studies");
    }
    public void language() {
        System.out.println("Language improvement classes");
    }

    public abstract void additional();
}
