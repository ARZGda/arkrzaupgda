package pl.org.pfig;

import pl.org.basicComputer.Laptop;
import pl.org.basicComputer.MidiTowerComputer;

public class Main {

    public static void main(String[] args) {

        SingletonExample se1 = SingletonExample.getInstance();
        se1.setName("Arkadiusz");
        SingletonExample se2 = SingletonExample.getInstance();
        System.out.println(se2.getName());

        System.out.println();

        Laptop laptop = new Laptop();
        System.out.println("Laptop");
        laptop.devices();

        MidiTowerComputer mtc = new MidiTowerComputer();
        System.out.println("Midi Tower");
        mtc.devices();


    }
}
