package pl.org.pfig;

/**
 * Created by RENT on 2017-06-22.
 */
public class SingletonExample {

    private static SingletonExample _instance;
    public String name;
    private SingletonExample() {
    }

    public static SingletonExample getInstance() {
        if (_instance == null) {
            System.out.println("Tworze instancje.");
            _instance = new SingletonExample();
        }

        System.out.println("Zwracam instancje, bo jest juz utworzona.");
        return _instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
