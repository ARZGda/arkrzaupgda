package pl.org.CouponFactory;

import java.util.Random;

/**
 * Created by RENT on 2017-06-22.
 */
public class Main {
    public static void main(String[] args) {
        int[] numbers = new int[6];
        for(int i = 0; i<numbers.length;i++){
            numbers[i] = new Random().nextInt(48) +1;
        }
        Coupon c = CouponFactory.getCoupon(numbers);
        System.out.println(c.toString());
    }
}
