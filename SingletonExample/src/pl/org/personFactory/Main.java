package pl.org.personFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by RENT on 2017-06-22.
 */
public class Main {

    public static void main(String[] args) {
        List<Person> people = new LinkedList<>();

        people.add(new Person("Pawel", "Testowy",30));
        people.add(new Person("Piotr", "Testowy",33));
        people.add(new Person("Adam", "Randomowy",31));
        people.add(new Person("Piotr", "Przykladowy",32));

        People pp = new People().addGroup("Staff", people);
        for( Person pers : pp.from("staff").name("Pawel").get()) {
            System.out.println(pers);
        }
    }
}
