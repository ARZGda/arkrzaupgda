package pl.org.pfig.main;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
	private String name;
	private String secondName;
	private Address address;
	private String phone;
	private UserSex sex;
	private String CCN;
	private String PESEL;
	private Date birthDate;
	
	public User(){}

	public User(String name, String secondName, Address address, String phone, UserSex sex, String cCN, String pESEL,
			Date birthDate) {
		super();
		this.name = name;
		this.secondName = secondName;
		this.address = address;
		this.phone = phone;
		this.sex = sex;
		CCN = cCN;
		PESEL = pESEL;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public UserSex getSex() {
		return sex;
	}

	public void setSex(UserSex sex) {
		this.sex = sex;
	}

	public String getCCN() {
		return CCN;
	}

	public void setCCN(String cCN) {
		CCN = cCN;
	}

	public String getPESEL() {
		return PESEL;
	}

	public void setPESEL(String pESEL) {
		PESEL = pESEL;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY");
		return "User [name=" + name + ", secondName=" + secondName + ", address=" + address + ", phone=" + phone
				+ ", sex=" + sex + ", CCN=" + CCN + ", PESEL=" + PESEL + ", birthDate=" + sdf.format(birthDate) + "]";
	};
	
	
}
