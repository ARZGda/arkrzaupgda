package pl.org.pfig.main;

public class Address {

	private String street;
	private String zipCode;
	private String number;
	private String city;
	private String country;
	
	public Address() {}

	public Address(String street, String zipCode, String number, String city, String country) {
		super();
		this.street = street;
		this.zipCode = zipCode;
		this.number = number;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "[street=" + street + ", number=" + number + ", zipCode=" + zipCode + ", city=" + city
				+ ", country=" + country + "]";
	}
	
	
}
