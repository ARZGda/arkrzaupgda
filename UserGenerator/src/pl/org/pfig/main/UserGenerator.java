package pl.org.pfig.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	private final String path = "resources/";
	private User currentUser;
	private Address currentAddress;
	public User getRandomUser(){
		UserSex us = UserSex.SEX_MALE;
		if(new Random().nextInt(2) == 0){
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us);
	}
	
	public Address getRandomAddress(){
		currentAddress = new Address();
		currentAddress.setStreet(getRandomLineFromFile("street.txt"));
		String[] hometown = getRandomLineFromFile("city.txt").split(" \t ");
		currentAddress.setCity(hometown[0]);
		currentAddress.setZipCode(hometown[1]);
		currentAddress.setNumber(""+ (new Random().nextInt(100)+1));
		currentAddress.setCountry("Poland");
		return currentAddress;
	}
	
	public User getRandomUser(UserSex sex) {
		currentUser = new User();
		currentUser.setSex(sex);
		String name= "";
		if(sex.equals(UserSex.SEX_FEMALE)){
			name = getRandomLineFromFile("name_f.txt");
		}else{
			name=getRandomLineFromFile("name_m.txt");
		}
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		currentUser.setAddress(getRandomAddress());
		String dt = getRandomBirthDate();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		}catch (ParseException e){
			System.out.println(e.getMessage());
		}
		currentUser.setPESEL(generatePesel());
		return currentUser;
	}
	
	private String getRandomBirthDate() {
			int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
			int year = 1890 + new Random().nextInt(120);
			int month = new Random().nextInt(12)+1;
			if ((year % 4 ==0 && year % 100 != 0)||year%400 == 0) {
				daysInMonths[1]++;
			}
			int day = new Random().nextInt(daysInMonths[month-1])+1;
		return leadZero(day)+"."+leadZero(month)+"."+year;
	}
	private String generatePesel(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY");
		String dateTab = sdf.format(currentUser.getBirthDate());
		String pesel = "";
		if (dateTab.charAt(7)=='9'){
			pesel =""+ dateTab.charAt(8) + dateTab.charAt(9) + dateTab.charAt(3) + dateTab.charAt(4) + dateTab.charAt(0)+dateTab.charAt(1);
		} else if (dateTab.charAt(7)=='8'){
			pesel =""+ dateTab.charAt(8) + dateTab.charAt(9) + (8+Integer.parseInt(""+dateTab.charAt(3))) + dateTab.charAt(4) + dateTab.charAt(0)+dateTab.charAt(1);
		}else if (dateTab.charAt(7)=='0'){
			pesel =""+ dateTab.charAt(8) + dateTab.charAt(9) + (2+Integer.parseInt(""+dateTab.charAt(3))) + dateTab.charAt(4) + dateTab.charAt(0)+dateTab.charAt(1);
		}
		int genderInt = new Random().nextInt(9999)+1;
		if ((genderInt % 2 == 1 && currentUser.getSex()==UserSex.SEX_FEMALE)||(genderInt % 2 == 0 && currentUser.getSex()==UserSex.SEX_MALE)){
			genderInt--;
		}
		if (100<= genderInt && genderInt < 1000){
			pesel = pesel + "0" + genderInt;
		} else if (10 <= genderInt && genderInt < 100) {
		pesel = pesel +"00"+ genderInt;
		} else if (genderInt <10) {
			pesel = pesel+"000"+genderInt;
		} else {
			pesel = pesel + genderInt;
		}
		System.out.println(pesel);
		return pesel;
	}
	
	private String leadZero(int arg) {
		if (arg<10){
			return "0"+arg;
		} else return "" + arg;
	}
	
	private int countLines(String filename){
		int lines = 0;
		try {
			Scanner sc = new Scanner(new File(path + filename));
			while(sc.hasNextLine()){
				lines++;
				sc.nextLine();
			}
			return lines;
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}
	
	private String getRandomLineFromFile(String filename) {
		int maxLines = countLines(filename);
		Random generator = new Random();
		int choosenLine = generator.nextInt(maxLines);
		int line = 0;
		try {
		Scanner sc = new Scanner(new File(path + filename));
		while (line != choosenLine){
			line++;
			sc.nextLine();
		}
		return sc.nextLine();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return "Generator couldn't find propper line";
	}
}
