import java.util.Random;

/**
 * Created by RENT on 2017-06-27.
 */
public class Sorter {

    public static void main(String[] args) {

        Random random = new Random();
        int[] data = new int[10];
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(10000);
        }
        //   Sorter.sort(data);
        //   Sorter.sortByPut(data);
        // Sorter.sortBySelect(data);
        Sorter.mergeSort(data);
        for (int i = 0; i < data.length; i++) {
            System.out.println("" + data[i]);
        }


    }

    public static int[] sort(int[] data) {
        int isSorted = 1;
        int buffer;
        int end = data.length - 1;
        while (isSorted != 0) {
            isSorted = 0;
            for (int i = 0; i < end; i++) {
                if (data[i] > data[i + 1]) {
                    buffer = data[i];
                    data[i] = data[i + 1];
                    data[i + 1] = buffer;
                    isSorted = 1;
                }
            }
            end--;
        }

        return data;
    }

    public static int[] sortByPut(int[] data) {

        for (int i = 0; i < data.length; i++) {
            int current = data[i];
            int position = 0;
            while (data[position] < current && position <= i) {
                position++;
            }
            while (position <= i) {
                int moved = data[position];
                data[position] = current;
                current = moved;
                position++;
            }
        }
        return data;
    }

    public static int[] sortBySelect(int[] data) {

        return data;
    }

    public static void mergeSort(int[] data) {
        if (data.length > 1) {
            int middle = data.length / 2;
            int[] left = copy(0, middle - 1, data);
            int[] right = copy(middle, data.length - 1, data);
            mergeSort(left);
            mergeSort(right);
            merge(left, right, data);
        }
    }

    public static void merge(int[] left, int[] right, int[] data) {
        int a = 0;
        int b = 0;
        int i = 0;

        while (a < left.length && b < right.length) {
            if (left[a] < right[b]) {
                data[i] = left[a];
                a++;
            } else {
                data[i] = right[b];
                b++;
            }
            i++;
        }
        while (a < left.length) {
            data[i] = left[a];
            i++;
            a++;
        }
        while (b < right.length) {
            data[i] = right[b];
            i++;
            b++;
        }


    }

    public static int[] copy(int start, int end, int[] data) {
        int j = 0;
        for (int i = start; i <= end; i++) {
            data[j++] = data[i];
        }
        return data;
    }
}
