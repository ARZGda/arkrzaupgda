CREATE DATABASE countryCapitol;

CREATE TABLE state (
    id INT AUTO_INCREMENT PRIMARY KEY,
   name varchar(30),
   population INT,
   capital_id INT, FOREIGN KEY(capital_id) REFERENCES city(id)
   );
   
CREATE TABLE City (
    id INT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(30),
   citizens INT
   );
   
INSERT INTO city (name,citizens) VALUES
('Praga',1280500),
('Paryż',2000000),
('Wilno',539900),
('Berlin',3400000),
('Warszawa',1753977),
('Olsztyn',173500),
('Gdańsk',463700),
('Sopot',37650),
('Gdynia',247500),
('Kraków',762500);

INSERT INTO state (name, population,capital_id) VALUES
('Czechy',10627448,1),
('Francja', 66259012,2),
('Litwa',3505738,3),
('Niemcy',80996685,4),
('Polska',38346279,5);